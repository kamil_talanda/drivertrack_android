package com.bluetill.drivertrack_xt;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 31/01/14
 * Time: 09:31
 * To change this template use File | Settings | File Templates.
 */
public abstract class MainActivity extends FragmentActivity {
    private Typeface fontAwesome;

    public Typeface getFontAwesome() {
        return fontAwesome;
    }

    public MainActivity getClassElement() {
        return MainActivity.this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fontAwesome = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");
    }
}
