package com.bluetill.drivertrack_xt.location;

/**
 * Created with IntelliJ IDEA.
 * User: kamil
 * Date: 18.07.13
 * Time: 09:21
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.provider.Settings;
import android.util.Log;
import com.bluetill.drivertrack_xt.R;
import com.bluetill.drivertrack_xt.Vars;

import static com.bluetill.drivertrack_xt.Globals.APPTAG;

/**
 * Defines app-wide constants and utilities
 */
public final class LocationUtils implements Vars {

    public static final int MILLISECONDS_PER_SECOND = 1000;
    public static final int UPDATE_INTERVAL_IN_SECONDS = 5;
    public static final int SYNC_INTERVAL = DEBUG_MODE ? 3 : 12;      //sync every SYNC_INTERVAL * UPDATE_INTERVAL seconds

//    public static boolean servicesConnected(Activity activity) {
//        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity);
//        if (ConnectionResult.SUCCESS == resultCode) {
//            Log.d(Utils.APPTAG,
//                    "Google Play services is available.");
//            return true;
//        } else {
//            Log.d(Utils.APPTAG,
//                    "Google Play services is not available.");
//            return false;
//        }
//    }

    public static boolean checkGPS(Activity activity, final Context context) {
        boolean gps_enabled = false;
        try {
            gps_enabled = ((LocationManager) context.getSystemService(Context.LOCATION_SERVICE)).isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception e) {
            Log.d(APPTAG, "LocationManager exception");
            e.printStackTrace();
        }

        if (!gps_enabled) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setTitle(activity.getString(R.string.alertTitle));
            builder.setMessage(activity.getString(R.string.gpsNotEnabledAlert));
            builder.setPositiveButton(activity.getString(R.string.enableButton), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(myIntent);
                }
            });
            builder.setNegativeButton(context.getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
            return false;
        } else {
            return true;
        }

    }
}