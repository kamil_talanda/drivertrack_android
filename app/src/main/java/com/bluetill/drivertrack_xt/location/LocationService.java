package com.bluetill.drivertrack_xt.location;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;
import com.bluetill.drivertrack_xt.DriverTrackActivity;
import com.bluetill.drivertrack_xt.R;
import com.bluetill.drivertrack_xt.Vars;
import com.bluetill.drivertrack_xt.webService.WSResponse;
import com.bluetill.drivertrack_xt.webService.WebServiceTask;
import com.bluetill.drivertrack_xt.webService.WebServiceUtil;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.bluetill.drivertrack_xt.Globals.*;

/**
 * Created with IntelliJ IDEA.
 * User: kamil
 * Date: 24.07.13
 * Time: 17:08
 * To change this template use File | Settings | File Templates.
 */
public class LocationService extends Service implements Vars{

    private int driverId = -1;
    private LocationService locationService;
    private static LocationGA locationGA;
    private static boolean isFirst = true;
    private CollectLocationTask collectLocationTask;

    @Override
    public void onCreate() {
        super.onCreate();
        this.locationService = this;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        this.driverId = intent.getIntExtra(DRIVER_PARAMETER_ID, -1);

        Intent baseIntent = new Intent(getBaseContext(), DriverTrackActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(getBaseContext(), 0, baseIntent, 0);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(getString(R.string.app_title))
                .setSmallIcon(R.drawable.icon);
        mBuilder.setContentIntent(pIntent);
        NotificationManager notificationManager = (NotificationManager)
                getSystemService(NOTIFICATION_SERVICE);
        startForeground(NEW_MESSAGE_NOTIFICATION_ID, mBuilder.build());

        this.locationGA = new LocationGA(this);
        this.locationGA.connect();
        this.collectLocationTask = new CollectLocationTask(this);
        this.collectLocationTask.execute();
        globalLocationService = this;
        return Service.START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        this.driverId = -1;
        this.collectLocationTask.cancel(true);
        this.locationGA.disconnect();
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public Location getLocation() {
        return this.locationGA.getLocation();
    }

    public String getLocationProvider() {
        return this.locationGA.getLocationProvider();
    }

    public void syncDriver(final ArrayList<Directions> pointList) {

        WebServiceTask syncTask = new WebServiceTask(WS_SYNC_DRIVER, getApplicationContext(), null) {
            @Override
            protected void onPostExecute(WSResponse wsResponse) {
                boolean collectingFailed = false;

                if (!this.isCancelled() && driverId != -1) {
                    try {
                        if (wsResponse.getStatusCode() == WebServiceUtil.WS_STATUS_OK) {
                            if (!wsResponse.getResponseString().equals("")) {
                                getMessage(wsResponse.getResponseString());
                            }
                            if (pointList.size() > 0) {
                                collectLocationTask = new CollectLocationTask(locationService, pointList.get(pointList.size() - 1));
                            } else {
                                collectLocationTask = new CollectLocationTask(locationService);
                            }
                        } else {
                            collectingFailed = true;

                        }
                    } catch (NullPointerException e) {
                        collectingFailed = true;
                    }
                    if (collectingFailed) {
                        if (pointList.size() > 7200) {
                            for (int i = 0; i < LocationUtils.SYNC_INTERVAL; i++) {
                                pointList.remove(0);
                            }
                        }
                        collectLocationTask = new CollectLocationTask(locationService, pointList);
                    }
                    collectLocationTask.execute();
                }
                super.onPostExecute(wsResponse);
            }
        };
        String lineString = "";
        if (pointList.size() > 1) {
            lineString = "LINESTRING(";
            for (Directions directions : pointList) {
                lineString += Double.toString(directions.getLongitude());
                lineString += " ";
                lineString += Double.toString(directions.getLatitude());
                lineString += ",";
            }
            lineString = lineString.substring(0, lineString.length() - 1);
            lineString += ")";
        }
        if (pointList.size() == 1 && isFirst) {
            lineString = "POINT(";
            for (Directions directions : pointList) {
                lineString += Double.toString(directions.getLongitude());
                lineString += " ";
                lineString += Double.toString(directions.getLatitude());
                lineString += ",";
            }
            lineString = lineString.substring(0, lineString.length() - 1);
            lineString += ")";
        }
        JSONObject json = new JSONObject();
        try {
            json.put(JSON_USERNAME, username);
            json.put(JSON_DRIVER_ID, driverId);
            json.put(JSON_LINE, lineString);
            json.put(JSON_ANDROID_ID, DriverTrackActivity.getAndroid_id());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        isFirst = false;
        syncTask.execute(json);
    }

    public void getMessage(String responseString) {
        DriverTrackActivity.addMessage(responseString);
        String msgShort = responseString;

        if (responseString.length() > 10) {
            msgShort = responseString.substring(0, 10);
        }

        Intent baseIntent = new Intent(getBaseContext(), DriverTrackActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(getBaseContext(), 0, baseIntent, 0);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(locationService)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(getString(R.string.newMsg) + " - " + msgShort + "...")
                .setSmallIcon(R.drawable.icon);
        mBuilder.setContentIntent(pIntent);

        long[] pattern = {500, 500, 500, 500, 500, 500, 500, 500, 500};
        mBuilder.setVibrate(pattern);
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mBuilder.setSound(alarmSound);
        NotificationManager notificationManager = (NotificationManager)
                getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(COLLECT_LOCATION_SERVICE_ID, mBuilder.build());

        Toast.makeText(getApplicationContext(), getString(R.string.app_name) + " - " + getString(R.string.newMsg), Toast.LENGTH_SHORT).show();
    }
}