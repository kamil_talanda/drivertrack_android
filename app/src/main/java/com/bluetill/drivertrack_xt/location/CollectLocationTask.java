package com.bluetill.drivertrack_xt.location;

import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;
import com.bluetill.drivertrack_xt.R;
import com.bluetill.drivertrack_xt.Vars;

import java.util.ArrayList;

import static com.bluetill.drivertrack_xt.Globals.*;


/**
 * Created with IntelliJ IDEA.
 * User: kamil
 * Date: 02.08.13
 * Time: 13:15
 */
public class CollectLocationTask extends AsyncTask<Void, Boolean, Void> implements Vars {
    private ArrayList<Directions> pointList;
    private LocationService locationService;
    public static String locationState = null;

    public CollectLocationTask(LocationService locationService, Directions offsetDirections) {
        this.pointList = new ArrayList<Directions>();
        if (offsetDirections != null) {
            pointList.add(offsetDirections);
        }
        this.locationService = locationService;
    }

    public CollectLocationTask(LocationService locationService, ArrayList<Directions> pointList) {
        this.pointList = pointList;
        this.locationService = locationService;
    }

    public CollectLocationTask(LocationService locationService) {
        this.pointList = new ArrayList<Directions>();
        this.locationService = locationService;
    }

    @Override
    protected void onProgressUpdate(Boolean... values) {
        super.onProgressUpdate(values);
        String msg = values[0] ? this.locationService.getLocationProvider() : this.locationService.getString(R.string.app_name) + " - " + this.locationService.getString(R.string.no_location);
        CollectLocationTask.locationState = msg;
        //Toast.makeText(this.locationService.getApplicationContext(), msg, Toast.LENGTH_LONG).show();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            for (int i = 0; i < LocationUtils.SYNC_INTERVAL; i++) {
                Thread.sleep(LocationUtils.MILLISECONDS_PER_SECOND * LocationUtils.UPDATE_INTERVAL_IN_SECONDS);
                Location location = locationService.getLocation();

                Location lastLocation = null;
                if (this.pointList.size() > 0) {
                    lastLocation = new Location("gps");
                    lastLocation.setLatitude(this.pointList.get(this.pointList.size() - 1).getLatitude());
                    lastLocation.setLongitude(this.pointList.get(this.pointList.size() - 1).getLongitude());
                }

                if (lastLocation != null && location != null) {
                    float[] distanceResults = new float[1];
                    Location.distanceBetween(location.getLatitude(), location.getLongitude(), lastLocation.getLatitude(), lastLocation.getLongitude(), distanceResults);
                    float distanceBetweenLocations = distanceResults[0];
                    //check if user moved less than x meters in last 5s
                    if (distanceBetweenLocations < 25) {
                        this.pointList.remove(this.pointList.size() - 1);
                        location = lastLocation;
                    }

//                    //check if user moved more than x meters in last 5s
//                    if (distanceBetweenLocations > 500 && this.pointList.size() > 0) {
//                        this.pointList.remove(this.pointList.size() - 1);
//                    }
                }

                if (location != null) {
                    this.pointList.add(new Directions(location.getLongitude(), location.getLatitude()));
                    if (DEBUG_MODE) {
                        publishProgress(true);
                    }
                } else {
                    publishProgress(false);
                }
            }
            locationService.syncDriver(this.pointList);
        } catch (InterruptedException e) {
            Log.d(APPTAG, "collectLocationTask " + e.toString());
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }
};