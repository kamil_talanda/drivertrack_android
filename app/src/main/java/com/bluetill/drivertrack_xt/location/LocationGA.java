package com.bluetill.drivertrack_xt.location;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;


/**
 * Created with IntelliJ IDEA.
 * User: kamil
 * Date: 17.07.13
 * Time: 11:07
 * To change this template use File | Settings | File Templates.
 */
public class LocationGA implements LocationListener {
    private LocationService locationService;
    Location location;
    private LocationManager locationManager;
    private String provider;

    public LocationGA(LocationService locationService) {
        this.locationService = locationService;
        this.provider = "gps";
    }

    @Override
    public void onLocationChanged(Location location) {
        this.location = location;
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
    }

    @Override
    public void onProviderEnabled(String s) {
    }

    @Override
    public void onProviderDisabled(String s) {
    }

    public Location getLocation() {
        this.location = this.locationManager.getLastKnownLocation(this.provider);
        return this.location;
    }

    public String getLocationProvider() {
        return this.provider;
    }

    public void connect() {
        this.locationManager = (LocationManager) locationService.getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        this.location = this.locationManager.getLastKnownLocation(this.provider);
        this.locationManager.requestLocationUpdates(this.provider, LocationUtils.MILLISECONDS_PER_SECOND * LocationUtils.UPDATE_INTERVAL_IN_SECONDS, 1, this);
    }

    public void disconnect() {
        locationManager.removeUpdates(this);
    }


}
