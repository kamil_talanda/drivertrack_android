package com.bluetill.drivertrack_xt.location;

/**
 * Created with IntelliJ IDEA.
 * User: kamil
 * Date: 17.07.13
 * Time: 08:08
 * To change this template use File | Settings | File Templates.
 */
public class Directions {
    public Directions(double longitude, double latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }


    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    double longitude;
    double latitude;
}
