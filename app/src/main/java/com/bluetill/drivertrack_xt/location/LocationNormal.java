package com.bluetill.drivertrack_xt.location;

import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;

import static com.bluetill.drivertrack_xt.Globals.APPTAG;

public class LocationNormal extends Service implements LocationListener {

    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000*5*1; // 5 seconds

    private final Context mContext;
    boolean isGPSEnabled = false;
    boolean isNetworkEnabled = false;
    protected LocationManager locationManager;

    public LocationNormal(Context context) {
        this.mContext = context;
    }

    public Location getLocation() {
        Location location = null;
        try {
            this.locationManager = (LocationManager) this.mContext
                    .getSystemService(LOCATION_SERVICE);
            if (this.canGetLocation()) {
                if (this.isNetworkEnabled) {
                    this.locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            this.MIN_TIME_BW_UPDATES,
                            this.MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    Log.d("Network", "Network");
                    if (this.locationManager != null) {
                        location = this.locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    }
                }
                if (this.isGPSEnabled) {
                    this.locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            this.MIN_TIME_BW_UPDATES,
                            this.MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    Log.d("GPS Enabled", "GPS Enabled");
                    if (this.locationManager != null) {
                        location = this.locationManager
                                .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    }
                }
            }

        } catch (Exception e) {
            Log.d(APPTAG, "LocationManager exception");
            e.printStackTrace();
        }

        return location;
    }

    public void stopUsingGPS() {
        if (this.locationManager != null) {
            this.locationManager.removeUpdates(LocationNormal.this);
        }
    }

    public boolean canGetLocation() {
        this.locationManager = (LocationManager)this.mContext
                .getSystemService(LOCATION_SERVICE);
        this.isGPSEnabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
        this.isNetworkEnabled = locationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        return this.isGPSEnabled || this.isNetworkEnabled;
    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
        alertDialog.setTitle("GPS settings");
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                mContext.startActivity(intent);
            }
        });
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    @Override
    public void onLocationChanged(Location location) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

}