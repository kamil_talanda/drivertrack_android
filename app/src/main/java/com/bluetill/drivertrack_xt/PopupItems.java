package com.bluetill.drivertrack_xt;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.text.InputType;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.bluetill.drivertrack_xt.schedule.list.barcodeList.ScheduleBarcodeListActivity;
import com.bluetill.drivertrack_xt.schedule.list.mainList.ScheduleMainListActivity;
import com.google.zxing.integration.android.IntentIntegrator;

/**
 * Created by ktalanda on 13/02/14.
 */
public class PopupItems implements Vars {

    Activity context;
    View view;
    boolean isMain;


    public PopupItems(Activity context, View view, boolean isMain) {
        this.context = context;
        this.view = view;
        this.isMain = isMain;
    }

    public void showPopupLegacy(){
        if(isMain){
            Intent intent = new Intent(context, ScheduleMainListActivity.class);
            context.startActivity(intent);
        }else{
            IntentIntegrator scanIntegrator = new IntentIntegrator(context);
            scanIntegrator.initiateScan();
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void showPopup11() {
        android.widget.PopupMenu popup = new android.widget.PopupMenu(context, view);
        MenuInflater inflater = popup.getMenuInflater();
        if(isMain) inflater.inflate(R.menu.item_menu, popup.getMenu());
        else inflater.inflate(R.menu.item_small_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new android.widget.PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.list:
                        Intent intent = new Intent(context, ScheduleMainListActivity.class);
                        context.startActivity(intent);
                        return true;
                    case R.id.scan:
                        IntentIntegrator scanIntegrator = new IntentIntegrator(context);
                        scanIntegrator.initiateScan();
                        return true;
                    case R.id.type_in:
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle(context.getString(R.string.type_in_window_title));

                        final EditText input = new EditText(context);

                        input.setInputType(InputType.TYPE_CLASS_NUMBER);
                        builder.setView(input);

                        builder.setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent itemIntent = new Intent(context, ScheduleBarcodeListActivity.class);
                                itemIntent.putExtra(ITEM_PARAMETER_BARCODE, input.getText().toString());
                                context.startActivity(itemIntent);
                            }
                        });
                        builder.setNegativeButton(context.getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                        builder.show();
                        return true;
                    default:
                        return false;
                }
            }
        });
        popup.show();
    }
}

