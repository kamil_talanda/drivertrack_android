package com.bluetill.drivertrack_xt;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.bluetill.drivertrack_xt.webService.WSResponse;
import com.bluetill.drivertrack_xt.webService.WebServiceTask;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created with IntelliJ IDEA.
 * User: kamil
 * Date: 18.07.13
 * Time: 12:32
 */
public class PairActivity extends Activity implements Vars{

    public static final String SHARED_PREFERENCE_DRIVER_NAME = "driver_name";
    public static final String SHARED_PREFERENCE_USERNAME = "username";

    private Button pairButton;
    private EditText username;
    private EditText driverName;
    private TextView appVersion;
    private Intent returnIntent = new Intent();
    private Activity pairActivity;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pair);
        this.pairActivity = this;
        sharedPreferences = pairActivity.getPreferences(Context.MODE_PRIVATE);

        this.appVersion = (TextView) findViewById(R.id.app_version);
        try {
            this.appVersion.setText(getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        this.pairButton = (Button) findViewById(R.id.rightButton);
        this.username = (EditText) findViewById(R.id.pair_username);
        this.driverName = (EditText) findViewById(R.id.pair_drivername);

        this.username.setText(getStringFromSharedPreferences(SHARED_PREFERENCE_USERNAME));
        this.driverName.setText(getStringFromSharedPreferences(SHARED_PREFERENCE_DRIVER_NAME));

        this.pairButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (driverName.getText()!=null && !driverName.getText().toString().equals("")) {
                    WebServiceTask task = new WebServiceTask("users_signIn", getApplicationContext(), pairActivity) {
                        @Override
                        protected void onPostExecute(WSResponse wsResponse) {
                            this.connectDialog.dismiss();
                            if (wsResponse != null) {
                                try {
                                    if (Integer.parseInt(wsResponse.getResponseString()) == -1) {
                                        Toast.makeText(getApplicationContext(), getString(R.string.noUser), Toast.LENGTH_LONG).show();
                                    } else {
                                        int driverIdTmp = Integer.parseInt(wsResponse.getResponseString());
                                        String driverNameTmp = driverName.getText().toString().toLowerCase();
                                        String usernameTmp = username.getText().toString().toLowerCase();

                                        setStringToSharedPreferences(SHARED_PREFERENCE_USERNAME, usernameTmp);
                                        setStringToSharedPreferences(SHARED_PREFERENCE_DRIVER_NAME, driverNameTmp);

                                        returnIntent.putExtra(DRIVER_PARAMETER_ID, driverIdTmp);
                                        returnIntent.putExtra(DRIVER_PARAMETER_NAME, driverNameTmp);
                                        setResult(RESULT_OK, returnIntent);
                                        Globals.driverName = driverName.getText().toString().toLowerCase();
                                        Globals.username = username.getText().toString().toLowerCase();
                                        finish();
                                    }
                                } catch (Exception e) {
                                    Toast.makeText(getApplicationContext(), getString(R.string.connection_fail), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), getString(R.string.connection_fail), Toast.LENGTH_SHORT).show();
                            }
                            super.onPostExecute(wsResponse);
                        }
                    };

                    JSONObject json = new JSONObject();
                    try {
                        json.put(JSON_USERNAME, username.getText().toString().toLowerCase());
                        json.put(JSON_DRIVER_NAME, driverName.getText().toString().toLowerCase());
                        json.put(JSON_ANDROID_ID, DriverTrackActivity.getAndroid_id());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    task.execute(json);
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.empty_user), Toast.LENGTH_SHORT).show();
                }
            }
        });
        //TODO: debug
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    protected void onStart() {
        super.onStart();    //To change body of overridden methods use File | Settings | File Templates.
    }

    @Override
    protected void onRestart() {
        super.onRestart();    //To change body of overridden methods use File | Settings | File Templates.
    }

    @Override
    protected void onResume() {
        super.onResume();    //To change body of overridden methods use File | Settings | File Templates.
    }

    @Override
    protected void onStop() {
        super.onStop();    //To change body of overridden methods use File | Settings | File Templates.
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();    //To change body of overridden methods use File | Settings | File Templates.
    }

    private void setStringToSharedPreferences(String preference, String driverName) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(preference, driverName);
        editor.commit();
    }

    private String getStringFromSharedPreferences(String preference) {
        return sharedPreferences.getString(preference, "");
    }
}
