package com.bluetill.drivertrack_xt.webService;

import android.app.Activity;
import android.app.ProgressDialog;

/**
 * Created with IntelliJ IDEA.
 * User: kamil
 * Date: 18.07.13
 * Time: 18:08
 */
public class WebServiceUtil {

    public static final int WS_STATUS_OK = 200;

    public static ProgressDialog setConnectDialog(Activity activity) {
        ProgressDialog progressDialog = new ProgressDialog(activity);
        progressDialog.setTitle("Connecting...");
        progressDialog.setMessage("Please wait.");
        progressDialog.setCancelable(false);
        return progressDialog;
    }
}