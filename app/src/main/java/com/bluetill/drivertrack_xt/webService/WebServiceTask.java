package com.bluetill.drivertrack_xt.webService;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.bluetill.drivertrack_xt.R;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

/**
 * Created with IntelliJ IDEA.
 * User: kamil
 * Date: 06.08.13
 * Time: 11:53
 */
public class WebServiceTask extends AsyncTask<JSONObject, Integer, WSResponse> {
    protected ProgressDialog connectDialog;
    private Context applicationContext;
    private String wsName;
    private Activity activity;

    public WebServiceTask(String wsName, Context applicationContext, Activity activity) {
        this.wsName = wsName;
        this.applicationContext = applicationContext;
        this.activity = activity;
    }

    @Override
    protected WSResponse doInBackground(JSONObject... json) {
        return this.wsConnect(applicationContext, json[0], wsName);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (activity != null) {
            this.connectDialog = WebServiceUtil.setConnectDialog(activity);
            this.connectDialog.show();
        }
    }

    public WSResponse wsConnect(Context context, JSONObject json, String webService) {
        WSResponse wsResponse = null;

        HttpClient client = new DefaultHttpClient();
        HttpConnectionParams.setConnectionTimeout(client.getParams(), 2000); //Timeout Limit
        HttpResponse response;

        try {
            HttpPost post = new HttpPost(context.getResources().getString(R.string.WSaddress) + webService);

            StringEntity se = new StringEntity(json.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            response.getEntity().writeTo(out);
            out.close();
            wsResponse = new WSResponse(response.getStatusLine().getStatusCode(),out.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return wsResponse;
    }


}
