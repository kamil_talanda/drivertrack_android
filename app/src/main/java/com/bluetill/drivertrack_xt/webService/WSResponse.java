package com.bluetill.drivertrack_xt.webService;

/**
 * Created with IntelliJ IDEA.
 * User: kamil
 * Date: 15.08.13
 * Time: 14:41
 * To change this template use File | Settings | File Templates.
 */
public class WSResponse {
    private int statusCode;
    private String responseString;

    public WSResponse(int statusCode, String responseString) {
        this.statusCode = statusCode;
        this.responseString = responseString;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getResponseString() {
        return responseString;
    }
}
