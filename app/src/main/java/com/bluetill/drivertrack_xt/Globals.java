package com.bluetill.drivertrack_xt;

import com.bluetill.drivertrack_xt.location.LocationService;
import com.bluetill.drivertrack_xt.schedule.ScheduleItemListCache;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 04/12/13
 * Time: 12:16
 * To change this template use File | Settings | File Templates.
 */
public class Globals {
    public static final String APPTAG = "DriverTrackActivity - location module";

    public static int driverId = -1;
    public static LocationService globalLocationService = null;

    public static ScheduleItemListCache itemListCache = new ScheduleItemListCache(true);

    public static String username = "";
    public static String driverName = "";

}
