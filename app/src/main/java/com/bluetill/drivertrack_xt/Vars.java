package com.bluetill.drivertrack_xt;

/**
 * Created by ktalanda on 02/12/14.
 */
public interface Vars {

    int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    int LOGIN_ACTIVITY_REQUEST = 1000;
    int SIGN_ACTIVITY = 1001;
    int ITEM_ACTIVITY = 1002;

    int COLLECT_LOCATION_SERVICE_ID = 2301;
    int NEW_MESSAGE_NOTIFICATION_ID = 2302;

    //TODO: change debug mode
    boolean DEBUG_MODE = true;

    /*Driver passed parameters*/
    String DRIVER_PARAMETER_ID = "driverId";
    String DRIVER_PARAMETER_NAME = "driverName";

    /*JSON field names*/
    /*driver*/
    String JSON_USERNAME = "username";
    String JSON_ANDROID_ID = "android_id";

    String JSON_DRIVER_NAME = "driver_name";

    /*location*/
    String JSON_DRIVER_ID = "driver_id";
    String JSON_LINE = "line";


    /* ScheduleItemActivity passed parameters*/
    String ITEM_PARAMETER_ID = "id";
    String ITEM_PARAMETER_BARCODE = "barcode";
    String ITEM_PARAMETER_NAME = "name";
    String ITEM_PARAMETER_DESTINATION = "destination";
    String ITEM_PARAMETER_DESCRIPTION = "description";
    String ITEM_PARAMETER_SIGNATURE_IMAGE = "signature_image";
    String ITEM_PARAMETER_STATUS = "status";
    String ITEM_PARAMETER_ITEM_LIST = "item_list";
    String ITEM_PARAMETER_HOLDER = "holder_id";
    String ITEM_APPROVED = "approved";

    String WS_BASE_ITEMS = "items";
    String WS_GET_ITEM_LIST = "getItemList";
    String WS_GET_ITEM = "getItem";
    String WS_GET_ITEM_LIST_BARCODE = "getItemByBarcode";
    String WS_APPROVE_ITEM = "approveItem";
    String WS_ADD_DESCRIPTION = "addDescription";
    String WS_SET_HOLDER = "setHolder";
    String WS_RESET_HOLDER = "resetHolder";

    String WS_BASE_POSITION = "position";
    String WS_SYNC_DRIVER = WS_BASE_POSITION + "_syncDriver";

}
