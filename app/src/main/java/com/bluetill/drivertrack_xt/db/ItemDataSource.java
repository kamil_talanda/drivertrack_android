package com.bluetill.drivertrack_xt.db;

/**
 * Created with IntelliJ IDEA.
 * User: kamil
 * Date: 16.08.13
 * Time: 10:24
 * To change this template use File | Settings | File Templates.
 */
public class ItemDataSource {

//    // Database fields
//    private SQLiteDatabase database;
//    private OfflineSQLiteHelper dbHelper;
//
//    public ItemDataSource(Context context) {
//        this.dbHelper = new OfflineSQLiteHelper(context);
//        database = dbHelper.getWritableDatabase();
//    }
//
//    public void closeDB(){
//        database.close();
//    }
//
//    public void addItem(String id, String name, String destination, String description, String status, String signatureImage) {
//        ContentValues values = new ContentValues();
//        if (!doesTableExist()) {
//            database.execSQL(ITEM_DATABASE_CREATE);
//        }
//        values.put(ITEM_COLUMN_ID, id);
//        values.put(ITEM_COLUMN_NAME, name);
//        values.put(ITEM_COLUMN_DESTINATION, destination);
//        values.put(ITEM_COLUMN_DESCRIPTION, description);
//        values.put(ITEM_COLUMN_STATUS, status);
//        values.put(ITEM_COLUMN_SIGNATURE_IMAGE, signatureImage);
//        long insertId = database.insert(ITEM_TABLE_NAME, null,
//                values);
//
//    }
//
//    public void dropTable() {
//        database.execSQL("DROP TABLE IF EXISTS " + ITEM_TABLE_NAME);
//    }
//
//    public boolean doesTableExist() {
//        Cursor cursor = database.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='" + ITEM_TABLE_NAME + "'", null);
//        try {
//            if (cursor.getCount() > 0) {
//                return true;
//            }
//        } catch (NullPointerException e) {
//            return false;
//        }
//        return false;
//    }
//
//    public ArrayList<ScheduleItem> getItemList() {
//        ArrayList<ScheduleItem> itemArrayList = new ArrayList<>();
//
//
//        Cursor cursor = database.rawQuery("select * from " + ITEM_TABLE_NAME, null);
//        cursor.moveToFirst();
//
//        while (!cursor.isAfterLast()) {
//            String id = "";
//            String name = "NOT_FOUND";
//            String destination = "";
//            String description = "";
//            String status = "DONE";
//            String signatureImage = "";
//
//            id = cursor.getString(0);
//            name = cursor.getString(1);
//            destination = cursor.getString(2);
//            status = cursor.getString(4);
//            itemArrayList.add(new ScheduleItem(id, name, destination, description, status, signatureImage, ""));
//            cursor.moveToNext();
//        }
//
//        return itemArrayList;
//    }
//
//    public ArrayList<ScheduleItem> getItemListBrief() {
//        ArrayList<ScheduleItem> itemArrayList = new ArrayList<>();
//
//        Cursor cursor = database.rawQuery("select " + ITEM_COLUMN_ID + ", " + ITEM_COLUMN_NAME + ", " + ITEM_COLUMN_DESTINATION + ", " + ITEM_COLUMN_STATUS + " from " + ITEM_TABLE_NAME, null);
//        cursor.moveToFirst();
//
//        while (!cursor.isAfterLast()) {
//            String id = "";
//            String name = "NOT_FOUND";
//            String destination = "";
//            String description = "";
//            String status = "DONE";
//            String signatureImage = "";
//
//            id = cursor.getString(0);
//            name = cursor.getString(1);
//            destination = cursor.getString(2);
//            status = cursor.getString(3);
//            itemArrayList.add(new ScheduleItem(id, name, destination, description, status, signatureImage, ""));
//            cursor.moveToNext();
//        }
//
//        return itemArrayList;
//    }
//
//    public ScheduleItem getItem(String itemId) {
//        ArrayList<ScheduleItem> itemArrayList = new ArrayList<>();
//        String id = itemId;
//        String name = "NOT_FOUND";
//        String destination = "";
//        String description = "";
//        String status = "DONE";
//        String signatureImage = "";
//
//        Cursor cursor = database.rawQuery("select * from " + ITEM_TABLE_NAME + " where " + ITEM_COLUMN_ID + "='" + itemId + "'", null);
//        cursor.moveToFirst();
//        if (!cursor.isAfterLast()) {
//            id = cursor.getString(0);
//            name = cursor.getString(1);
//            destination = cursor.getString(2);
//            description = cursor.getString(3);
//            status = cursor.getString(4);
//            signatureImage = cursor.getString(5);
//        }
//
//        return new ScheduleItem(id, name, destination, description, status, signatureImage, "");
//    }
//
//    public ScheduleItem getItemBrief(String itemId) {
//        ArrayList<ScheduleItem> itemArrayList = new ArrayList<>();
//        String id = itemId;
//        String name = "NOT_FOUND";
//        String destination = "";
//        String description = "";
//        String status = "DONE";
//        String signatureImage = "";
//
//        Cursor cursor = database.rawQuery("select " + ITEM_COLUMN_NAME + ", "  + ITEM_COLUMN_DESTINATION + ", "  + ITEM_COLUMN_STATUS + " from " + ITEM_TABLE_NAME + " where " + ITEM_COLUMN_ID + "='" + itemId + "'", null);
//        cursor.moveToFirst();
//        if (!cursor.isAfterLast()) {
//            name = cursor.getString(0);
//            destination = cursor.getString(1);
//            status = cursor.getString(2);
//        }
//
//        return new ScheduleItem(id, name, destination, description, status, signatureImage, "");
//    }
//
//    public ScheduleItem getItemDetails(String itemId) {
//        ArrayList<ScheduleItem> itemArrayList = new ArrayList<>();
//        String id = itemId;
//        String name = "NOT_FOUND";
//        String destination = "";
//        String description = "";
//        String status = "DONE";
//        String signatureImage = "";
//
//        Cursor cursor = database.rawQuery("select " + ITEM_COLUMN_DESCRIPTION + ", "  + ITEM_COLUMN_SIGNATURE_IMAGE + " from " + ITEM_TABLE_NAME + " where " + ITEM_COLUMN_ID + "='" + itemId + "'", null);
//        cursor.moveToFirst();
//        if (!cursor.isAfterLast()) {
//            destination = cursor.getString(0);
//            signatureImage = cursor.getString(1);
//        }
//
//        return new ScheduleItem(id, name, destination, description, status, signatureImage, "");
//    }
//
//    public void approveItem(String itemId, String signatureImage) {
//        //database.rawQuery("update " + ITEM_TABLE_NAME + " set "+ ITEM_COLUMN_STATUS + " = '" + ITEM_STATUS_DONE + "', "+ ITEM_COLUMN_SIGNATURE_IMAGE + " = '"+ "asd" + "' where " + ITEM_COLUMN_ID + "=ad'" + itemId + "'", null);
//        ContentValues contentValues = new ContentValues();
//        contentValues.put(ITEM_COLUMN_STATUS, ITEM_STATUS_DONE);
//        contentValues.put(ITEM_COLUMN_SIGNATURE_IMAGE, signatureImage);
//        database.update(ITEM_TABLE_NAME, contentValues,"id='"+itemId+"'",null);
//    }
//
//    public void changeDescriptionLine(String itemId, String changedDescription){
//        ContentValues contentValues = new ContentValues();
//        contentValues.put(ITEM_COLUMN_DESCRIPTION, changedDescription);
//        database.update(ITEM_TABLE_NAME, contentValues,"id='"+itemId+"'",null);
//    }

}
