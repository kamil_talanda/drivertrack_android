package com.bluetill.drivertrack_xt.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.bluetill.drivertrack_xt.location.Directions;

import java.util.ArrayList;

import static com.bluetill.drivertrack_xt.db.OfflineSQLiteHelper.*;
/**
 * Created with IntelliJ IDEA.
 * User: kamil
 * Date: 16.08.13
 * Time: 10:24
 * To change this template use File | Settings | File Templates.
 */
public class LocationDataSource {

    // Database fields
    private SQLiteDatabase database;
    private OfflineSQLiteHelper dbHelper;
    private String[] allColumns = {LOCATION_COLUMN_ID,
            LOCATION_COLUMN_LONGITUDE, LOCATION_COLUMN_LATITUDE};

    public LocationDataSource(Context context) {
        this.dbHelper = new OfflineSQLiteHelper(context);
        database = dbHelper.getWritableDatabase();
    }

    public void addPoints(ArrayList<Directions> pointList) {
        ContentValues values = new ContentValues();
        if(!doesTableExist()){
            database.execSQL(LOCATION_DATABASE_CREATE);
        }
        for (Directions point : pointList) {
            values.put(LOCATION_COLUMN_LONGITUDE, point.getLongitude());
            values.put(LOCATION_COLUMN_LATITUDE, point.getLatitude());
            long insertId = database.insert(LOCATION_TABLE_NAME, null,
                    values);
            String a = "" + insertId;
            int b = a.length();
        }
    }

    public void dropTable() {
        database.execSQL("DROP TABLE IF EXISTS " + LOCATION_TABLE_NAME);
    }

    public boolean doesTableExist() {
        Cursor cursor = database.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='" + LOCATION_TABLE_NAME + "'", null);
        try {
            if (cursor.getCount() > 0) {
                return true;
            }
        } catch (NullPointerException e) {
            return false;
        }
        return false;
    }

}
