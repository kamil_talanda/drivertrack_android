package com.bluetill.drivertrack_xt.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created with IntelliJ IDEA.
 * User: kamil
 * Date: 16.08.13
 * Time: 10:11
 * To change this template use File | Settings | File Templates.
 */
public class OfflineSQLiteHelper extends SQLiteOpenHelper {

    public static final String LOCATION_TABLE_NAME = "comments";
    public static final String LOCATION_COLUMN_ID = "_id";
    public static final String LOCATION_COLUMN_LONGITUDE = "longitude";
    public static final String LOCATION_COLUMN_LATITUDE = "latitude";

    public static final String ITEM_TABLE_NAME = "items";
    public static final String ITEM_COLUMN_ID = "id";
    public static final String ITEM_COLUMN_NAME = "name";
    public static final String ITEM_COLUMN_DESTINATION = "destination";
    public static final String ITEM_COLUMN_DESCRIPTION = "description";
    public static final String ITEM_COLUMN_STATUS = "status";
    public static final String ITEM_COLUMN_SIGNATURE_IMAGE = "signature_image";

    private static final String DB_NAME = "driverTrack_offline.db";
    private static final int DB_VERSION = 1;

    // Database creation sql statement
    public static final String LOCATION_DATABASE_CREATE = "create table "
            + LOCATION_TABLE_NAME + "(" + LOCATION_COLUMN_ID
            + " integer primary key autoincrement, " + LOCATION_COLUMN_LONGITUDE
            + " real, " + LOCATION_COLUMN_LATITUDE + " real);";

    public static final String ITEM_DATABASE_CREATE = "create table "
            + ITEM_TABLE_NAME + "("
            + ITEM_COLUMN_ID + " text, "
            + ITEM_COLUMN_NAME + " text, "
            + ITEM_COLUMN_DESTINATION  + " text,"
            + ITEM_COLUMN_DESCRIPTION  + " text,"
            + ITEM_COLUMN_STATUS  + " text,"
            + ITEM_COLUMN_SIGNATURE_IMAGE + " text);";

    public OfflineSQLiteHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(LOCATION_DATABASE_CREATE);
        sqLiteDatabase.execSQL(ITEM_DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
        sqLiteDatabase.execSQL("");
    }
}
