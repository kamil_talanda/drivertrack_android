package com.bluetill.drivertrack_xt;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import java.io.ByteArrayOutputStream;

/**
 * Created with IntelliJ IDEA.
 * User: kamil
 * Date: 07.08.13
 * Time: 12:35
 */
public class Utils {

    /* Base64 support functions */
    public static Bitmap decodeBase64Bmp(String base64Image){
        byte[] b = Base64.decode(base64Image, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(b, 0, b.length);
    }

    public static String encodeBase64Bmp(Bitmap bitmap){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
        byte[] b = baos.toByteArray();
        return Base64.encodeToString(b,Base64.DEFAULT);
    }

}
