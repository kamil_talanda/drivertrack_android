package com.bluetill.drivertrack_xt;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.Settings;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bluetill.drivertrack_xt.location.LocationService;
import com.bluetill.drivertrack_xt.location.LocationUtils;
import com.bluetill.drivertrack_xt.schedule.ItemUtils;
import com.bluetill.drivertrack_xt.schedule.ItemVars;
import com.bluetill.drivertrack_xt.schedule.list.mainList.ScheduleMainListActivity;
import com.google.zxing.integration.android.IntentIntegrator;

import java.util.Calendar;

import static com.bluetill.drivertrack_xt.Globals.APPTAG;
import static com.bluetill.drivertrack_xt.Globals.driverId;
import static com.bluetill.drivertrack_xt.Globals.driverName;

/**
 * Created with IntelliJ IDEA.
 * User: kamil
 * Date: 18.07.13
 * Time: 09:21
 */

public class DriverTrackActivity extends MainActivity implements ItemVars {

    private static Button unpairButton;
    private static Button menuButton;
    private static TextView nameText;
    private static TextView idText;
    private static TextView msgText;
    private static View msgSeparator;
    private static View logoDT;
    private static View separator1;
    private static TextView app_version;
    private static ImageButton pairDeviceButton;
    private static String android_id;
    private Intent service;
    private DriverTrackActivity activity;


    public static String getAndroid_id() {
        return android_id;
    }

    public static void addMessage(String msg) {
        Calendar c = Calendar.getInstance();
        String date = c.getTime().toString();
        String allMsg = msgText.getText() + "\n--" + date + "----------\n" + msg;
        msgText.setText(allMsg);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        DriverTrackActivity.android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);

        setContentView(R.layout.main);
        this.service = new Intent(getApplicationContext(), LocationService.class);
        this.activity = this;

        this.unpairButton = (Button) findViewById(R.id.rightButton);
        this.menuButton = (Button) findViewById(R.id.leftButton);
        this.nameText = (TextView) findViewById(R.id.nameText);
        this.idText = (TextView) findViewById(R.id.idText);
        this.msgText = (TextView) findViewById(R.id.msgText);
        this.pairDeviceButton = (ImageButton) findViewById(R.id.pairButton);
        this.msgSeparator = (View) findViewById(R.id.separator_2);
        this.logoDT = (View) findViewById(R.id.logo_dt);
        this.separator1 = (View) findViewById(R.id.separator_1);
        this.app_version = (TextView) findViewById(R.id.app_version);

        this.msgText.setMovementMethod(new ScrollingMovementMethod());
        try {
            this.app_version.setText(getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if (driverId == -1) {
            setMenuForNoUser();
            if (LocationUtils.checkGPS(activity, getApplicationContext())) {
                setDriver(-1);
            }

        } else {
            setMenuForUser();
        }

        this.unpairButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(DriverTrackActivity.this);
                builder.setTitle(getString(R.string.alertTitle));
                builder.setMessage(getString(R.string.logOutAlertTitle));

                builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        setDriver(-1);
                        Globals.itemListCache.clearCache();
                    }
                });

                builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
        this.pairDeviceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (LocationUtils.checkGPS(activity, getApplicationContext())) {
                    setDriver(-1);
                }
            }
        });
        this.menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ScheduleMainListActivity.class);
                startActivity(intent);
//                showPopup(view);

            }
        });

        //TODO: debug
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

    }


    public void showPopup(View view) {
        PopupItems popupItems = new PopupItems(this, view, true);
        if (android.os.Build.VERSION.SDK_INT >= 11) {
            popupItems.showPopup11();
        } else {
            popupItems.showPopupLegacy();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        NotificationManager notificationManager = (NotificationManager)
                getSystemService(NOTIFICATION_SERVICE);
        notificationManager.cancel(COLLECT_LOCATION_SERVICE_ID);
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        NotificationManager notificationManager = (NotificationManager)
                getSystemService(NOTIFICATION_SERVICE);
        notificationManager.cancel(COLLECT_LOCATION_SERVICE_ID);
        stopService(service);
        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_HOME) {
            if (driverId == -1) {
                finish();
            } else {
                moveTaskToBack(true);
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {

        ItemUtils.scanBarcodeAction(IntentIntegrator.parseActivityResult(requestCode, resultCode, intent), this);

        switch (requestCode) {
            case LOGIN_ACTIVITY_REQUEST:
                if (resultCode == RESULT_OK) {
                    driverName = intent.getStringExtra(DRIVER_PARAMETER_NAME.toString());
                    setDriver(intent.getIntExtra(DRIVER_PARAMETER_ID, -1));
                }
                if (resultCode == RESULT_CANCELED) {
                }
                break;
            case CONNECTION_FAILURE_RESOLUTION_REQUEST:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        break;
                    default:
                        break;
                }
            default:
                Log.d(APPTAG,
                        getString(R.string.unknown_activity_error));
                break;
        }
    }

    public void setDriver(int newDriverId) {
        driverId = newDriverId;
        if (driverId == -1) {
            getApplicationContext().stopService(service);
            setMenuForNoUser();
            Intent intent = new Intent(this, PairActivity.class);
            startActivityForResult(intent, LOGIN_ACTIVITY_REQUEST);

        } else {
            setMenuForUser();
            service.putExtra(DRIVER_PARAMETER_ID, newDriverId);
            getApplicationContext().startService(service);
        }
    }

    public void setMenuForUser() {
        this.nameText.setText(getString(R.string.hello) + " " + driverName);
        this.idText.setText(getString(R.string.id_description) + Integer.toString(driverId));
        this.nameText.setVisibility(View.VISIBLE);
        this.idText.setVisibility(View.VISIBLE);
        this.unpairButton.setVisibility(View.VISIBLE);
        this.menuButton.setVisibility(View.VISIBLE);
        this.msgText.setVisibility(View.VISIBLE);
        this.msgSeparator.setVisibility(View.VISIBLE);
        this.logoDT.setVisibility(View.VISIBLE);
        this.separator1.setVisibility(View.VISIBLE);
        this.msgText.setText("");
        this.pairDeviceButton.setVisibility(View.GONE);

    }

    public void setMenuForNoUser() {
        this.nameText.setVisibility(View.GONE);
        this.idText.setVisibility(View.GONE);
        this.unpairButton.setVisibility(View.GONE);
        this.menuButton.setVisibility(View.GONE);
        this.msgText.setVisibility(View.GONE);
        this.msgSeparator.setVisibility(View.GONE);
        this.logoDT.setVisibility(View.GONE);
        this.separator1.setVisibility(View.GONE);
        this.msgText.setText("");
        this.pairDeviceButton.setVisibility(View.VISIBLE);
    }
}