package com.bluetill.drivertrack_xt.schedule.list.mainList;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import com.bluetill.drivertrack_xt.R;
import com.bluetill.drivertrack_xt.schedule.ItemStatus;
import com.bluetill.drivertrack_xt.schedule.ItemUtils;
import com.bluetill.drivertrack_xt.schedule.ItemVars;
import com.bluetill.drivertrack_xt.schedule.ScheduleItem;
import com.bluetill.drivertrack_xt.schedule.item.ScheduleItemActivity;
import com.bluetill.drivertrack_xt.schedule.list.ScheduleListBaseActivity;
import com.bluetill.drivertrack_xt.schedule.list.barcodeList.ScheduleBarcodeListActivity;
import com.bluetill.drivertrack_xt.schedule.list.sort.SortType;
import com.google.zxing.integration.android.IntentIntegrator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import static com.bluetill.drivertrack_xt.Globals.APPTAG;
import static com.bluetill.drivertrack_xt.Globals.itemListCache;


/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 26/11/13
 * Time: 19:17
 * To change this template use File | Settings | File Templates.
 */
public class ScheduleMainListActivity extends ScheduleListBaseActivity implements ItemVars {

    {
        if (filterItem.getStatusList().size() == 0) {
            filterItem.getStatusList().add(ItemStatus.IN_PROGRESS.toValue());
            filterItem.getStatusList().add(ItemStatus.EMPTY.toValue());
            filterItem.getStatusList().add(ItemStatus.DONE.toValue());
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        rightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                approveManyAction(itemsToApprove, true);
//                IntentIntegrator scanIntegrator = new IntentIntegrator(ScheduleMainListActivity.this);
//                scanIntegrator.initiateScan();
            }
        });

        fillUpList();
    }



    @Override
    public void actionOnDataLoadingFinish() {
        setUpListView();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {

        ItemUtils.scanBarcodeAction(IntentIntegrator.parseActivityResult(requestCode, resultCode, intent), this);
        switch (requestCode) {
            case ITEM_ACTIVITY:
                if (resultCode == RESULT_OK) {
                    reloadItemList();
                }
                if (resultCode == RESULT_CANCELED) {
                }
                break;
            case SIGN_ACTIVITY:
                if (resultCode == RESULT_OK) {
                    ItemUtils.approveItemList(intent.getStringExtra(ITEM_PARAMETER_SIGNATURE_IMAGE), (ArrayList<Integer>) intent.getSerializableExtra(ITEM_PARAMETER_ITEM_LIST), this);
                    cleanUpAfterApproveMany((ArrayList<Integer>) intent.getSerializableExtra(ITEM_PARAMETER_ITEM_LIST));
                    reloadItemList();
                }
                if (resultCode == RESULT_CANCELED) {
                }
                break;
            default:
                Log.d(APPTAG,
                        getString(R.string.unknown_activity_error));
                break;
        }
    }

    @Override
    public void setUpListView() {
        final MainLazyAdapter tmpAdapter = new MainLazyAdapter(this, itemListCache.getItemList());
        itemListView.setAdapter(tmpAdapter);

        final ArrayList<ScheduleItem> visibleItemList = new ArrayList<ScheduleItem>();
        for (ScheduleItem scheduleItem : (ArrayList<ScheduleItem>) tmpAdapter.getData()) {
            for (Integer visibleStatus : filterItem.getStatusList()) {
                if (scheduleItem.getStatus() == visibleStatus) {
                    visibleItemList.add(scheduleItem);
                    break;
                }
            }
        }

        itemListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (!(visibleItemList.get(i)).isGroup()) {
                    Intent intent = new Intent(ScheduleMainListActivity.this, ScheduleItemActivity.class);
                    intent.putExtra(ITEM_PARAMETER_ID, visibleItemList.get(i).getId());
                    startActivityForResult(intent, ITEM_ACTIVITY);
                } else {
                    Intent itemIntent = new Intent(ScheduleMainListActivity.this, ScheduleBarcodeListActivity.class);
                    itemIntent.putExtra(ITEM_PARAMETER_BARCODE, visibleItemList.get(i).getBarcode());
                    startActivityForResult(itemIntent, ITEM_ACTIVITY);
                }
            }
        });
        adapter = tmpAdapter;
        filterList(filterItem);
        checkInProgress(false);
    }

    @Override
    public void resetListView() {
        final MainLazyAdapter tmpAdapter = (MainLazyAdapter) itemListView.getAdapter();
        itemListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (!((ScheduleItem) tmpAdapter.getData().get(i)).isGroup()) {
                    Intent intent = new Intent(ScheduleMainListActivity.this, ScheduleItemActivity.class);
                    intent.putExtra(ITEM_PARAMETER_ID, ((ScheduleItem) tmpAdapter.getData().get(i)).getId());
                    startActivityForResult(intent, ITEM_ACTIVITY);
                } else {
                    Intent itemIntent = new Intent(ScheduleMainListActivity.this, ScheduleBarcodeListActivity.class);
                    itemIntent.putExtra(ITEM_PARAMETER_BARCODE, ((ScheduleItem) tmpAdapter.getData().get(i)).getBarcode());
                    startActivityForResult(itemIntent, ITEM_ACTIVITY);
                }
            }
        });
    }

    public void sortList(final HashMap<SortType, Boolean> sortBy) {
        Collections.sort(itemListCache.getItemList(), new Comparator<ScheduleItem>() {
            @Override
            public int compare(ScheduleItem lhs, ScheduleItem rhs) {
                if (sortBy.keySet().contains(SortType.BARCODE)) {
                    return compareStringValue(sortBy, SortType.BARCODE, lhs.getBarcode(), rhs.getBarcode());
                } else if (sortBy.keySet().contains(SortType.ADD_TIME)) {
                    if (sortBy.get(SortType.ADD_TIME))
                        return lhs.getAddTime().compareTo(rhs.getAddTime());
                    else return rhs.getAddTime().compareTo(lhs.getAddTime());
                } else if (sortBy.keySet().contains(SortType.APPROVE_TIME)) {
                    if (lhs.getApproveTime() != null && rhs.getApproveTime() != null) {
                        if (sortBy.get(SortType.APPROVE_TIME)) {
                            return lhs.getApproveTime().compareTo(rhs.getApproveTime());
                        } else return rhs.getAddTime().compareTo(lhs.getAddTime());
                    }
                } else if (sortBy.keySet().contains(SortType.STATUS)) {
                    if (sortBy.get(SortType.STATUS))
                        return Integer.valueOf(lhs.getStatus()).compareTo(Integer.valueOf(rhs.getStatus()));
                    else
                        return Integer.valueOf(rhs.getStatus()).compareTo(Integer.valueOf(lhs.getStatus()));
                }
                return 0;
            }
        });
        filterList(filterItem);
    }

    private int compareStringValue(final HashMap<SortType, Boolean> sortBy, SortType sortType, String lhs, String rhs) {
        if (sortBy.get(sortType))
            return lhs.compareTo(rhs);
        else
            return rhs.compareTo(lhs);
    }

}