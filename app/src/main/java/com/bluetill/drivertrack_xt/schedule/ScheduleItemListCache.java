package com.bluetill.drivertrack_xt.schedule;

import java.util.ArrayList;

/**
 * Created by ktalanda on 13/02/14.
 */
public class ScheduleItemListCache implements ItemVars{
    private ArrayList<ScheduleItem> itemList;
    private long time;

    public ScheduleItemListCache(boolean isFirstTime){
        if(isFirstTime) this.time = 0;
        else this.time = System.currentTimeMillis();
        this.itemList = new ArrayList<ScheduleItem>();
    }

    public boolean isValid(){
        if(System.currentTimeMillis() - time > CACHE_VALID_TIME) return false;
        else return true;
    }

    public boolean isEmpty(){
        return getItemList()==null || getItemList().size()==0;
    }

    public void clearCache(){
        time = 0;
        itemList = new ArrayList<ScheduleItem>();
    }

    public void removeItem(ScheduleItem scheduleItem){
        itemList.remove(scheduleItem);
    }

    public ArrayList<ScheduleItem> getItemList(){
        return itemList;
    }
}
