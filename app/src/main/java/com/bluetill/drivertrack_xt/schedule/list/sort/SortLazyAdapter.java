package com.bluetill.drivertrack_xt.schedule.list.sort;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.bluetill.drivertrack_xt.R;
import com.bluetill.drivertrack_xt.schedule.list.LazyAdapter;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 29/01/14
 * Time: 11:51
 * To change this template use File | Settings | File Templates.
 */

public class SortLazyAdapter extends LazyAdapter {

    private ScheduleListSortDialog context;

    public SortLazyAdapter(Activity activity, ScheduleListSortDialog context) {
        this.activity = activity;
        ArrayList<SortType> data = new ArrayList<SortType>();
        data.add(SortType.SORT_BY);
        data.add(SortType.BARCODE);
        data.add(SortType.STATUS);
        data.add(SortType.ADD_TIME);
        this.data = data;
        this.inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (convertView == null) {
            view = inflater.inflate(R.layout.dialog_sort_list_row, null);
        }

        TextView sortType = (TextView) view.findViewById(R.id.sortType);
        sortType.setText(data.get(position).toString());

        final RadioGroup radioDirection = (RadioGroup) view.findViewById(R.id.radioDirection);
        final RadioButton growingButton = (RadioButton) view.findViewById(R.id.growing);
        final RadioButton descendingButton = (RadioButton) view.findViewById(R.id.descending);

        descendingButton.setVisibility(View.GONE);

        if(position==0){
            radioDirection.setEnabled(false);
            sortType.setText("BY");
            growingButton.setEnabled(false);
            growingButton.setText("UP");
            growingButton.setTextSize(10);
            descendingButton.setEnabled(false);
            descendingButton.setText("DOWN");
            descendingButton.setTextSize(10);
        } else{
            growingButton.setText("");
            descendingButton.setText("");
            final int curPosition = position;
            radioDirection.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    checkedChange(checkedId, curPosition);
                    context.reloadListView();
                }
            });

            if (ScheduleListSortDialog.sortBy.containsKey(data.get(position))) {
                radioDirection.setOnCheckedChangeListener(null);
                if (ScheduleListSortDialog.sortBy.get(data.get(position))) radioDirection.check(R.id.growing);
                else radioDirection.check(R.id.descending);
            }
        }
        return view;
    }

    private void checkedChange(int checkedId, int curPosition){
        if (checkedId == R.id.growing)
            ScheduleListSortDialog.setSortBy((SortType) data.get(curPosition), true);
        else ScheduleListSortDialog.setSortBy((SortType) data.get(curPosition), false);
    }

    @Override
    public Filter getFilter() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}

