package com.bluetill.drivertrack_xt.schedule.sign;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.bluetill.drivertrack_xt.R;
import com.bluetill.drivertrack_xt.schedule.ItemVars;
import com.bluetill.drivertrack_xt.schedule.ScheduleItem;

import static com.bluetill.drivertrack_xt.Utils.encodeBase64Bmp;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 26/11/13
 * Time: 21:57
 * To change this template use File | Settings | File Templates.
 */
public class SignActivity extends Activity implements ItemVars{
    private static TextView app_version;
    private static Button backButton;
    private static Button clearButton;
    private static Button approveButton;
    private static DrawingView signatureInput;

    private String signatureBase64Image;

    private Intent inIntent;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign);

        inIntent = getIntent();

        app_version = (TextView) findViewById(R.id.app_version);
        try {
            app_version.setText(getPackageManager().getPackageInfo(getPackageName(),0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        ScheduleItem scheduleItem = new ScheduleItem();
        scheduleItem.setSignatureImage(this.inIntent.getStringExtra(ITEM_PARAMETER_SIGNATURE_IMAGE));
        signatureBase64Image = this.inIntent.getStringExtra(ITEM_PARAMETER_SIGNATURE_IMAGE);

        signatureInput = (DrawingView) findViewById(R.id.signature_input);

        backButton = (Button) findViewById(R.id.leftButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SignActivity.this.finish();
            }
        });
        approveButton = (Button) findViewById(R.id.rightButton);
        approveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signatureBase64Image = encodeBase64Bmp(getBitmapFromView(signatureInput));
                Intent resultData = new Intent();
                resultData.putExtra(ITEM_PARAMETER_SIGNATURE_IMAGE, signatureBase64Image);
                resultData.putExtra(ITEM_PARAMETER_ITEM_LIST, inIntent.getSerializableExtra(ITEM_PARAMETER_ITEM_LIST));
                setResult(RESULT_OK,resultData);
                finish();
            }
        });
        clearButton = (Button) findViewById(R.id.centerButton);
        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signatureInput.clear();
            }
        });
    }

    public Bitmap getBitmapFromView(View view) {
        //Define a bitmap with the same size as the view
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(),Bitmap.Config.ARGB_8888);
        //Bind a canvas to it
        Canvas canvas = new Canvas(returnedBitmap);
        //Get the view's background
        Drawable bgDrawable =view.getBackground();
        if (bgDrawable!=null)
            //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
        else
            //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.WHITE);
        view.draw(canvas);
        return returnedBitmap;
    }
}