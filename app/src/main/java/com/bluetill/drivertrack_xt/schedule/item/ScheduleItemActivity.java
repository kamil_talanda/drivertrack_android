/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 26/11/13
 * Time: 21:25
 */

package com.bluetill.drivertrack_xt.schedule.item;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bluetill.drivertrack_xt.R;
import com.bluetill.drivertrack_xt.Globals;

import com.bluetill.drivertrack_xt.schedule.ItemStatus;
import com.bluetill.drivertrack_xt.schedule.ItemVars;
import com.bluetill.drivertrack_xt.schedule.ScheduleActivity;
import com.bluetill.drivertrack_xt.schedule.ScheduleItem;
import com.bluetill.drivertrack_xt.schedule.ScheduleService;
import com.bluetill.drivertrack_xt.schedule.sign.SignActivity;

import static com.bluetill.drivertrack_xt.Globals.APPTAG;
import static com.bluetill.drivertrack_xt.Globals.driverId;

public class ScheduleItemActivity extends ScheduleActivity implements ItemVars {

    protected TextView app_version;
    protected Button backButton;
    protected Button approveButton;
    protected TextView app_message;

    protected int itemId;
    protected int status;
    protected TextView descriptionText;
    protected TextView destinationText;
    protected Button addDescriptionButton;

    protected TextView holderText;
    protected TextView statusText;
    protected TextView barcodeText;
    protected ImageView barcodeImage;
    protected ImageView statusImage;
    protected ImageView signatureImage;

    private ScheduleItemActivityActions actions = new ScheduleItemActivityActions(this);

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.schedule_item);

        this.app_version = (TextView) findViewById(R.id.app_version);
        try {
            this.app_version.setText(getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        this.backButton = (Button) findViewById(R.id.leftButton);
        this.backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent resultData = new Intent();
                resultData.putExtra(ITEM_PARAMETER_STATUS, status);
                resultData.putExtra(ITEM_PARAMETER_ID, itemId);
                setResult(RESULT_OK, resultData);
                ScheduleItemActivity.this.finish();
            }
        });
        this.approveButton = (Button) findViewById(R.id.rightButton);
        this.approveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent outIntent = new Intent(ScheduleItemActivity.this, SignActivity.class);
                startActivityForResult(outIntent, SIGN_ACTIVITY);
            }
        });

        this.itemId = ITEM_NOT_FOUND_ID;
        this.status = ItemStatus.EMPTY.toValue();
        this.app_message = (TextView) findViewById(R.id.app_message);
        this.app_message.setText(getString(R.string.loading));
        this.destinationText = (TextView) findViewById(R.id.item_destination);
        this.descriptionText = (TextView) findViewById(R.id.item_description);
        this.addDescriptionButton = (Button) findViewById(R.id.item_description_add_button);
        this.holderText = (TextView) findViewById(R.id.item_holder);
        this.statusText = (TextView) findViewById(R.id.item_status);
        this.statusImage = (ImageView) findViewById(R.id.item_status_image);
        this.barcodeImage = (ImageView) findViewById(R.id.barcode_image);
        this.barcodeText = (TextView) findViewById(R.id.barcode_text);
        this.signatureImage = (ImageView) findViewById(R.id.signature_image);

        boolean isItemInCache = false;
        for (ScheduleItem scheduleItem : Globals.itemListCache.getItemList()) {
            if (scheduleItem.getId() == getIntent().getIntExtra(ITEM_PARAMETER_ID, ITEM_NOT_FOUND_ID) && scheduleItem.getSignatureImage() != null && !scheduleItem.getSignatureImage().equals("")) {
                setItem(scheduleItem);
                isItemInCache = true;
                break;
            }
        }
        if (!isItemInCache) {
            ScheduleService.context = getApplicationContext();
            ScheduleService.setScheduleItem(getIntent().getIntExtra(ITEM_PARAMETER_ID, ITEM_NOT_FOUND_ID), this);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void setItem(final ScheduleItem scheduleItem) {
        for (ScheduleItem scheduleCacheItem : Globals.itemListCache.getItemList()) {
            if (scheduleCacheItem.getId() == getIntent().getIntExtra(ITEM_PARAMETER_ID, ITEM_NOT_FOUND_ID)) {
                Globals.itemListCache.getItemList().remove(scheduleCacheItem);
                Globals.itemListCache.getItemList().add(scheduleItem);
                break;
            }
        }
        this.app_message.setText("");
        if (scheduleItem.getHolderId() != HOLDER_UNASSIGNED_ID && scheduleItem.getHolderId() != driverId) {
            this.descriptionText.setEnabled(false);
            this.signatureImage.setVisibility(View.GONE);
        }

        actions.initFields(scheduleItem);

        if (scheduleItem.getHolderId() != HOLDER_UNASSIGNED_ID) {
            if ((scheduleItem.getHolderId() == Globals.driverId)) {
                actions.enableUnassignAction(scheduleItem);
            } else {
                actions.disableApproveButton();
            }
        } else {
            if (scheduleItem.getId() != ITEM_NOT_FOUND_ID) {
                actions.enableAssignAction(scheduleItem);
            } else {
                actions.hideFieldCausedByItemNotFound();
            }
            actions.disableApproveButton();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        switch (requestCode) {
            case SIGN_ACTIVITY:
                if (resultCode == RESULT_OK) {
                    actions.updateItemCacheList(ITEM_PARAMETER_STATUS,ItemStatus.DONE.toValue());
                    actions.approveCurrentItem(intent.getStringExtra(ITEM_PARAMETER_SIGNATURE_IMAGE));
                }
                if (resultCode == RESULT_CANCELED) {
                }
                break;
            default:
                Log.d(APPTAG,
                        getString(R.string.unknown_activity_error));
                break;
        }
    }


    public ScheduleItemActivity getClassElement() {
        return ScheduleItemActivity.this;
    }


}