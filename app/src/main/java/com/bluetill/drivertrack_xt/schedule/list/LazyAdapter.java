package com.bluetill.drivertrack_xt.schedule.list;

import android.app.Activity;
import android.view.LayoutInflater;
import android.widget.BaseAdapter;
import android.widget.Filterable;
import com.bluetill.drivertrack_xt.schedule.ItemVars;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 29/01/14
 * Time: 13:56
 * To change this template use File | Settings | File Templates.
 */
public abstract class LazyAdapter extends BaseAdapter implements ItemVars, Filterable {
    protected static LayoutInflater inflater = null;
    protected Activity activity;
    protected ArrayList<?> data;

    protected static final SimpleDateFormat timeFormat = new SimpleDateFormat (TIME_FORMAT);

    public ArrayList<?> getData() {
        return data;
    }

    public void setData(ArrayList<?> data) {
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return this.data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
