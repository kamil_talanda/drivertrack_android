package com.bluetill.drivertrack_xt.schedule;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;

import com.bluetill.drivertrack_xt.DriverTrackActivity;
import com.bluetill.drivertrack_xt.Globals;
import com.bluetill.drivertrack_xt.R;
import com.bluetill.drivertrack_xt.schedule.item.ScheduleItemActivity;
import com.bluetill.drivertrack_xt.schedule.list.ScheduleListBaseActivity;
import com.bluetill.drivertrack_xt.webService.WSResponse;
import com.bluetill.drivertrack_xt.webService.WebServiceTask;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 01/12/13
 * Time: 12:03
 * To change this template use File | Settings | File Templates.
 */
public class ScheduleService implements ItemVars {

    public static Context context;

    public static void getScheduleItemList(final ScheduleActivity scheduleActivity) {
        executeWS(WS_GET_ITEM_LIST, createJSONQuery(null), null, scheduleActivity, false);
    }

    public static void getScheduleItemListByBarcode(String barcode, final ScheduleListBaseActivity scheduleListActivity) {
        HashMap<String, Object> queryMap = new HashMap<String, Object>();
        queryMap.put(WS_JSON_OUT_BARCODE, barcode);
        executeWS(WS_GET_ITEM_LIST_BARCODE, createJSONQuery(queryMap), null, scheduleListActivity, false);
    }

    public static void setScheduleItem(int itemId, final ScheduleItemActivity scheduleItemActivity) {
        HashMap<String, Object> queryMap = new HashMap<String, Object>();
        queryMap.put(WS_JSON_OUT_ITEM_ID, itemId);
        ScheduleItem item = new ScheduleItem();
        item.setId(itemId);
        executeWS(WS_GET_ITEM, createJSONQuery(queryMap), item, scheduleItemActivity, false);
    }

    public static void setScheduleItemByBarcode(String barcode, final ScheduleListBaseActivity scheduleListActivity) {
        HashMap<String, Object> queryMap = new HashMap<String, Object>();
        queryMap.put(WS_JSON_OUT_BARCODE, barcode);
        ScheduleItem item = new ScheduleItem();
        item.setBarcode(barcode);
        executeWS(WS_GET_ITEM_LIST_BARCODE, createJSONQuery(queryMap), item, scheduleListActivity, false);
    }

    public static void approveItem(ArrayList<Integer> itemId, String signatureImage, String approvePoint, ScheduleActivity scheduleActivity) {
        HashMap<String, Object> queryMap = new HashMap<String, Object>();
        queryMap.put(WS_JSON_OUT_ITEM_ID, itemId);
        queryMap.put(WS_JSON_OUT_SIGNATURE_IMAGE, signatureImage);
        queryMap.put(WS_JSON_OUT_POINT, approvePoint);
        executeWS(WS_APPROVE_ITEM, createJSONQuery(queryMap), null, scheduleActivity, false);
    }

    public static void changeDescriptionLine(int itemId, String changedDescription) {
        HashMap<String, Object> queryMap = new HashMap<String, Object>();
        queryMap.put(WS_JSON_OUT_ITEM_ID, itemId);
        queryMap.put(WS_JSON_OUT_DESCRIPTION, changedDescription);
        executeWS(WS_ADD_DESCRIPTION, createJSONQuery(queryMap), null, null, false);
    }

    public static void setHolder(int itemId, ScheduleItemActivity scheduleItemActivity) {
        HashMap<String, Object> queryMap = new HashMap<String, Object>();
        queryMap.put(WS_JSON_OUT_ITEM_ID, itemId);
        executeWS(WS_SET_HOLDER, createJSONQuery(queryMap), null, scheduleItemActivity, true);
    }

    public static void resetHolder(int itemId, ScheduleItemActivity scheduleItemActivity) {
        HashMap<String, Object> queryMap = new HashMap<String, Object>();
        queryMap.put(WS_JSON_OUT_ITEM_ID, itemId);
        executeWS(WS_RESET_HOLDER, createJSONQuery(queryMap), null, scheduleItemActivity, true);
    }

    public static JSONObject createJSONQuery(HashMap<String, Object> queryMap) {
        JSONObject jsonQuery = new JSONObject();
        try {
            jsonQuery.put(JSON_USERNAME, Globals.username);
            jsonQuery.put(WS_JSON_OUT_DRIVER_NAME, Globals.driverName);
            jsonQuery.put(WS_JSON_OUT_ANDROID_ID, DriverTrackActivity.getAndroid_id());
            if (queryMap != null) {
                Set<String> keys = queryMap.keySet();
                for (String key : keys) {
                    jsonQuery.put(key, queryMap.get(key));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonQuery;
    }

    public static void executeWS(final String nameWS, final JSONObject jsonObject, final ScheduleItem item, final ScheduleActivity scheduleActivity, final boolean finishActivity) {
        final WebServiceTask task = new WebServiceTask(nameWS, context, scheduleActivity) {

            @Override
            protected void onPostExecute(WSResponse wsResponse) {
                if (scheduleActivity != null) {
                    this.connectDialog.dismiss();
                }
                if (wsResponse != null) {
                    if (scheduleActivity != null) {
                        if(nameWS.equals(WS_APPROVE_ITEM)){
                            scheduleActivity.reloadItemList();
                        } else {
                            try {
                                String response = wsResponse.getResponseString().replace("[", "");
                                response = response.replace("]", "");
                                response = "[" + response + "]";
                                JSONArray jsonArray = new JSONArray(response);

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                                    ScheduleItem scheduleItem = ItemUtils.convertJsonToItem(jsonObject);
                                    if (item != null) {
                                        scheduleItem.copyNotEmpty(item);
                                    }
                                    scheduleActivity.setItem(scheduleItem);
                                }
                                scheduleActivity.actionOnDataLoadingFinish();

                            } catch (JSONException e) {
                                scheduleActivity.setItem(getErrorItem());
                            }
                        }
                    }

                } else {
                    if (scheduleActivity != null) {
                        scheduleActivity.setItem(getErrorItem());
                    }
                }
                if (finishActivity) {
                    Intent resultData = new Intent();
                    scheduleActivity.setResult(ScheduleActivity.RESULT_OK, resultData);
                    scheduleActivity.finish();
                }
                super.onPostExecute(wsResponse);
            }
        };
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, jsonObject);
        else
            task.execute(jsonObject);

    }

    private static ScheduleItem getErrorItem() {
        ScheduleItem errorItem = new ScheduleItem();
        errorItem.setId(ITEM_NOT_FOUND_ID);
        errorItem.setBarcode(context.getString(R.string.not_found));
        errorItem.setName(context.getResources().getString(R.string.not_found));
        errorItem.setStatus(ItemStatus.EMPTY.toValue());
        return errorItem;
    }

}