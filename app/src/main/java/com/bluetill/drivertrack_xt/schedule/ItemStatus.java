package com.bluetill.drivertrack_xt.schedule;

import com.bluetill.drivertrack_xt.R;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 26/11/13
 * Time: 21:17
 * To change this template use File | Settings | File Templates.
 */
public enum ItemStatus implements ItemVars {
    IN_PROGRESS(STATUS_IN_PROGRESS, 0, R.color.orange),
    DONE(STATUS_DONE, 1, R.color.green),
    EMPTY(STATUS_EMPTY, -1, R.color.gray);

    private String statusString;
    private int statusInt;
    private int colorId;

    ItemStatus(String statusString, int statusInt, int colorId){
        this.statusString = statusString;
        this.statusInt = statusInt;
        this.colorId = colorId;
    }

    public String toString(){
        return this.statusString;
    }

    public int toValue() {
        return statusInt;
    }

    public int getColorId() {
        return colorId;
    }
}
