package com.bluetill.drivertrack_xt.schedule;

import android.content.Intent;
import android.location.Location;
import com.bluetill.drivertrack_xt.MainActivity;
import com.bluetill.drivertrack_xt.Globals;
import com.bluetill.drivertrack_xt.location.Directions;
import com.bluetill.drivertrack_xt.schedule.list.barcodeList.ScheduleBarcodeListActivity;
import com.google.zxing.integration.android.IntentResult;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;


/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 14/01/14
 * Time: 14:12
 * To change this template use File | Settings | File Templates.
 */
public class ItemUtils implements ItemVars{

    public static ScheduleItem convertJsonToItem(JSONObject jsonObject) {

        ScheduleItem scheduleItem = new ScheduleItem();
        try {
            scheduleItem.setId(jsonObject.getInt(WS_JSON_IN_ITEM_ID));
        } catch (JSONException e) {
            scheduleItem.setId(-1);
        }
        try {
            scheduleItem.setBarcode(jsonObject.getString(WS_JSON_IN_BARCODE));
        } catch (JSONException e) {
            scheduleItem.setBarcode(null);
        }
        try {
            scheduleItem.setCarrier(jsonObject.getString(WS_JSON_IN_CARRIER));
        } catch (JSONException e) {
            scheduleItem.setCarrier(null);
        }
        try {
            scheduleItem.setTenant(jsonObject.getString(WS_JSON_IN_TENANT));
        } catch (JSONException e) {
            scheduleItem.setTenant(null);
        }
        try {
            scheduleItem.setQty(jsonObject.getInt(WS_JSON_IN_QTY));
        } catch (JSONException e) {
            scheduleItem.setQty(0);
        }
        try {
            scheduleItem.setHolderId(jsonObject.getInt(WS_JSON_IN_HOLDER_ID));
        } catch (JSONException e) {
            scheduleItem.setHolderId(0);
        }
        try {
            scheduleItem.setName(jsonObject.getString(WS_JSON_IN_NAME));
        } catch (JSONException e) {
            scheduleItem.setName(null);
        }
        try {
            scheduleItem.setDestination(jsonObject.getString(WS_JSON_IN_DESTINATION));
        } catch (JSONException e) {
            scheduleItem.setDestination(null);
        }
        try {
            scheduleItem.setAddTime(new Date(jsonObject.getLong(WS_JSON_IN_ADD_TIME)));
        } catch (JSONException e) {
            scheduleItem.setAddTime(null);
        }
        try {
            scheduleItem.setApproveTime(new Date(jsonObject.getLong(WS_JSON_IN_APPROVE_TIME)));
        } catch (JSONException e) {
            scheduleItem.setApproveTime(null);
        }
        try {
            scheduleItem.setDeliveryTimePlanned(new Date(jsonObject.getLong(WS_JSON_IN_DELIVERY_TIME_PLANNED)));
        } catch (JSONException e) {
            scheduleItem.setDeliveryTimePlanned(null);
        }
        try {
            scheduleItem.setApprovePoint(jsonObject.getString(WS_JSON_IN_APPROVE_POINT));
        } catch (JSONException e) {
            scheduleItem.setName(null);
        }
        try {
            scheduleItem.setSignatureImage(jsonObject.getString(WS_JSON_IN_SIGNATURE));
        } catch (JSONException e) {
            scheduleItem.setSignatureImage(null);
        }
        try {
            scheduleItem.setDescription(jsonObject.getString(WS_JSON_IN_DESCRIPTION));
        } catch (JSONException e) {
            scheduleItem.setDescription(null);
        }
        try {
            scheduleItem.setStatus(jsonObject.getInt(WS_JSON_IN_STATUS));
        } catch (JSONException e) {
            scheduleItem.setStatus(-1);
        }


        return scheduleItem;
    }

    public static void approveItemList(String signatureImage, ArrayList<Integer> tmpItemList, ScheduleActivity scheduleActivity) {
        String approvePointWKT = getApprovePointWKT(Globals.globalLocationService.getLocation());
        ScheduleService.approveItem(tmpItemList, signatureImage, approvePointWKT, scheduleActivity);
    }

    private static String getApprovePointWKT(Location lastLocation){
        String approvePointWKT = null;
        if (lastLocation != null) {
            Directions lastDirections = new Directions(lastLocation.getLatitude(), lastLocation.getLongitude());
            approvePointWKT = "POINT(";
            approvePointWKT += Double.toString(lastDirections.getLatitude());
            approvePointWKT += " ";
            approvePointWKT += Double.toString(lastDirections.getLongitude());
            approvePointWKT += ")";
        }
        return approvePointWKT;
    }

    public static void scanBarcodeAction(IntentResult scanningResult, MainActivity inActivity){
        if (scanningResult != null && scanningResult.getContents() != null) {
            Intent itemIntent = new Intent(inActivity.getClassElement(), ScheduleBarcodeListActivity.class);
            itemIntent.putExtra(ITEM_PARAMETER_BARCODE, scanningResult.getContents());
            inActivity.startActivityForResult(itemIntent, ITEM_ACTIVITY);
        }
    }
}
