package com.bluetill.drivertrack_xt.schedule.list;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bluetill.drivertrack_xt.Globals;
import com.bluetill.drivertrack_xt.PopupItems;
import com.bluetill.drivertrack_xt.R;
import com.bluetill.drivertrack_xt.schedule.ItemStatus;
import com.bluetill.drivertrack_xt.schedule.ItemVars;
import com.bluetill.drivertrack_xt.schedule.ScheduleActivity;
import com.bluetill.drivertrack_xt.schedule.ScheduleItem;
import com.bluetill.drivertrack_xt.schedule.ScheduleItemListCache;
import com.bluetill.drivertrack_xt.schedule.ScheduleService;
import com.bluetill.drivertrack_xt.schedule.list.barcodeList.PopupBarcodeApprove;
import com.bluetill.drivertrack_xt.schedule.list.filter.ScheduleListFilterDialog;
import com.bluetill.drivertrack_xt.schedule.list.filter.ScheduleListFilterItem;
import com.bluetill.drivertrack_xt.schedule.list.mainList.MainLazyAdapter;
import com.bluetill.drivertrack_xt.schedule.list.sort.ScheduleListSortDialog;
import com.bluetill.drivertrack_xt.schedule.list.sort.SortType;
import com.bluetill.drivertrack_xt.schedule.sign.SignActivity;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 30/01/14
 * Time: 09:54
 * To change this template use File | Settings | File Templates.
 */
public abstract class ScheduleListBaseActivity extends ScheduleActivity implements ItemVars {

    protected static ScheduleListFilterItem filterItem = new ScheduleListFilterItem();
    public ArrayList<Integer> itemsToApprove = new ArrayList<Integer>();
    private final ScheduleListBaseActivity thisContext = this;

    protected TextView app_version;
    protected TextView app_message;
    protected Button backButton;
    protected Button rightButton;

    protected TextView filterButton;
    protected TextView refreshButton;

    protected ListView itemListView;
    protected LazyAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.schedule_list);

        app_version = (TextView) findViewById(R.id.app_version);
        try {
            app_version.setText(getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        app_message = (TextView) findViewById(R.id.app_message);

        backButton = (Button) findViewById(R.id.leftButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent resultData = new Intent();
                setResult(RESULT_OK, resultData);
                getClassElement().finish();
            }
        });

        rightButton = (Button) findViewById(R.id.rightButton);

        itemListView = (ListView) findViewById(R.id.item_list);

        itemListView.setBackgroundColor(Color.WHITE);

        filterButton = (TextView) findViewById(R.id.middleButton);
        filterButton.setTypeface(getFontAwesome());
        filterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ScheduleListFilterDialog filterDialog = new ScheduleListFilterDialog();
                filterDialog.setContext(thisContext);
                filterDialog.show(getSupportFragmentManager(), null);
            }
        });

        refreshButton = (TextView) findViewById(R.id.refreshButton);
        refreshButton.setTypeface(getFontAwesome());
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reloadItemList();
            }
        });

        ScheduleListFilterItem inputFilterItem = new ScheduleListFilterItem();
        inputFilterItem.getStatusList().add(ItemStatus.IN_PROGRESS.toValue());
        filterList(inputFilterItem);
    }

    @Override
    protected void onResume() {
        super.onResume();
        itemsToApprove = new ArrayList<Integer>();
        setUpListView();
    }

    @Override
    public void setItem(ScheduleItem scheduleItem) {
        if (scheduleItem != null) {
            if (!Globals.itemListCache.getItemList().contains(scheduleItem))
                Globals.itemListCache.getItemList().add(scheduleItem);
            app_message.setText("");
        } else {
            app_message = (TextView) findViewById(R.id.app_message);
            app_message.setText(getString(R.string.listEmpty));
        }
        HashMap<SortType, Boolean> sortBy = new HashMap<SortType, Boolean>();
        sortBy.put(SortType.APPROVE_TIME, true);
        sortList(sortBy);
    }

    protected void fillUpList() {
        ScheduleService.context = getApplicationContext();
        if (Globals.itemListCache.isEmpty() || !Globals.itemListCache.isValid()) {
            reloadItemList();
        }
    }

    protected void fillUpList(String barcode) {
        ScheduleService.context = getApplicationContext();
        ScheduleService.getScheduleItemListByBarcode(barcode, this);
    }

    protected void addToList(String barcode) {
        boolean isAlreadyAdded = false;
        ArrayList<ScheduleItem> itemsToRemove = new ArrayList<ScheduleItem>();
        for (ScheduleItem item : Globals.itemListCache.getItemList()) {
            if (item.getBarcode().equals(barcode)) isAlreadyAdded = true;
        }
        if (!isAlreadyAdded) {
            fillUpList(barcode);
        } else {
            setUpListView();
        }
    }

    protected void checkInProgress(final boolean isWithPopup) {
        final ArrayList<Integer> itemsInProgressList = new ArrayList<Integer>();
        final LazyAdapter tmpAdapter = (LazyAdapter) itemListView.getAdapter();
        if (tmpAdapter != null) {
            for (ScheduleItem scheduleItem : (ArrayList<ScheduleItem>) tmpAdapter.getData()) {
                if (scheduleItem.getStatus() == ItemStatus.IN_PROGRESS.toValue())
                    itemsInProgressList.add(scheduleItem.getId());
            }
        }
    }

    protected void cleanUpAfterApproveMany(ArrayList<Integer> itemApproveList) {
        for (ScheduleItem scheduleItem : Globals.itemListCache.getItemList()) {
            if (itemApproveList.contains(Integer.valueOf(scheduleItem.getId())))
                scheduleItem.setStatus(ItemStatus.DONE.toValue());
        }
        itemsToApprove = new ArrayList<Integer>();
        filterList(filterItem);
    }

    public void filterList(ScheduleListFilterItem inputFilterItem) {
        MainLazyAdapter adapter = new MainLazyAdapter(this, Globals.itemListCache.getItemList());
        itemListView.setAdapter(adapter);
        filterItem = inputFilterItem;
        adapter.getFilter().filter(filterItem.toString());
        itemListView.setAdapter(adapter);
        this.adapter = adapter;
        resetListView();
    }

    public void approveManyAction(final ArrayList<Integer> itemList, boolean showAlert) {
        if (itemList.size() > 0) {
            if (showAlert) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(getClassElement());
                builder.setTitle(getString(R.string.alertTitle));
                builder.setMessage(getString(R.string.approveManyAlert));

                builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent outIntent = new Intent(getClassElement(), SignActivity.class);
                        outIntent.putExtra(ITEM_PARAMETER_ITEM_LIST, itemList);
                        startActivityForResult(outIntent, SIGN_ACTIVITY);
                    }
                });

                builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            } else {
                Intent outIntent = new Intent(getClassElement(), SignActivity.class);
                outIntent.putExtra(ITEM_PARAMETER_ITEM_LIST, itemList);
                startActivityForResult(outIntent, SIGN_ACTIVITY);
            }
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.nothingToApproveToast), Toast.LENGTH_LONG).show();
        }
    }

    public ScheduleListFilterItem getFilterItem() {
        return filterItem;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void showPopup(View view) {
        PopupBarcodeApprove popupBarcodeApprove = new PopupBarcodeApprove(this, view);
        if (android.os.Build.VERSION.SDK_INT >= 11) {
            popupBarcodeApprove.showPopup11();
        } else {
            popupBarcodeApprove.showPopupLegacy();
        }
    }

    protected void popUpMenuItem(View view) {
        PopupItems popupItems = new PopupItems(thisContext, view, false);
        if (android.os.Build.VERSION.SDK_INT >= 11) {
            popupItems.showPopup11();
        } else {
            popupItems.showPopupLegacy();
        }
    }

    public abstract void sortList(final HashMap<SortType, Boolean> sortBy);

    public abstract void setUpListView();

    public abstract void resetListView();

}
