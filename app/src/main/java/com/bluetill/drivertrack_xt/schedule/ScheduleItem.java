package com.bluetill.drivertrack_xt.schedule;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 26/11/13
 * Time: 20:31
 * To change this template use File | Settings | File Templates.
 */
public class ScheduleItem {

    private boolean isGroup = false;
    private int groupElementsNumber = 1;

    private int id = -1;
    private String barcode = null;
    private String carrier = null;
    private String tenant = null;
    private int qty = -1;
    private int holderId = -1;
    private String name = null;
    private String destination = null;
    private Date addTime = null;
    private Date approveTime = null;
    private Date deliveryTimePlanned = null;
    private String approvePoint = null;
    private String signatureImage = null;
    private String description = null;
    private int status = -1;

    public ScheduleItem() {
    }

    public ScheduleItem(ScheduleItem scheduleItem){
        this.copyNotEmpty(scheduleItem);
    }


    public ScheduleItem(int id, String barcode, String carrier, String tenant, int qty, int holderId, String name, String destination, Date addTime, Date approveTime, Date deliveryTimePlanned, String approvePoint, String signatureImage, String description, int status) {
        this.id = id;
        this.barcode = barcode;
        this.carrier = carrier;
        this.tenant = tenant;
        this.qty = qty;
        this.holderId = holderId;
        this.name = name;
        this.destination = destination;
        this.addTime = addTime;
        this.approveTime = approveTime;
        this.deliveryTimePlanned = deliveryTimePlanned;
        this.approvePoint = approvePoint;
        this.signatureImage = signatureImage;
        this.description = description;
        this.status = status;
    }

    public boolean isGroup() {
        return isGroup;
    }

    public void setGroup(boolean group) {
        isGroup = group;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public String getTenant() {
        return tenant;
    }

    public void setTenant(String tenant) {
        this.tenant = tenant;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getHolderId() {
        return holderId;
    }

    public void setHolderId(int holderId) {
        this.holderId = holderId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public Date getApproveTime() {
        return approveTime;
    }

    public void setApproveTime(Date approveTime) {
        this.approveTime = approveTime;
    }

    public Date getDeliveryTimePlanned() {
        return deliveryTimePlanned;
    }

    public void setDeliveryTimePlanned(Date deliveryTimePlanned) {
        this.deliveryTimePlanned = deliveryTimePlanned;
    }

    public String getApprovePoint() {
        return approvePoint;
    }

    public void setApprovePoint(String approvePoint) {
        this.approvePoint = approvePoint;
    }

    public String getSignatureImage() {
        return signatureImage;
    }

    public void setSignatureImage(String signatureImage) {
        this.signatureImage = signatureImage;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void increaseElementsNumber(){
        groupElementsNumber++;
    }

    public void decreaseElementsNumber(){
        groupElementsNumber--;
    }

    public int getGroupElementsNumber() {
        return groupElementsNumber;
    }

    public void copyNotEmpty(ScheduleItem scheduleItem) {
        this.setGroup(scheduleItem.isGroup());
        if (scheduleItem.getId() != -1)
            this.setId(scheduleItem.getId());
        if (scheduleItem.getBarcode() != null && !scheduleItem.getBarcode().equals(""))
            this.setBarcode(scheduleItem.getBarcode());
        if (scheduleItem.getCarrier() != null && !scheduleItem.getCarrier().equals(""))
            this.setCarrier(scheduleItem.getCarrier());
        if (scheduleItem.getTenant() != null && !scheduleItem.getTenant().equals(""))
            this.setTenant(scheduleItem.getTenant());
        if (scheduleItem.getQty() != -1)
            this.setQty(scheduleItem.getQty());
        if (scheduleItem.getHolderId() != -1)
            this.setHolderId(scheduleItem.getHolderId());

        if (scheduleItem.getName() != null && !scheduleItem.getName().equals(""))
            this.setName(scheduleItem.getName());

        if (scheduleItem.getDestination() != null && !scheduleItem.getDestination().equals(""))
            this.setDestination(scheduleItem.getDestination());
        if (scheduleItem.getAddTime() != null)
            this.setAddTime(scheduleItem.getAddTime());
        if (scheduleItem.getApproveTime() != null)
            this.setApproveTime(scheduleItem.getApproveTime());
        if (scheduleItem.getDeliveryTimePlanned() != null)
            this.setDeliveryTimePlanned(scheduleItem.getDeliveryTimePlanned());

        if (scheduleItem.getApprovePoint() != null && !scheduleItem.getApprovePoint().equals(""))
            this.setApprovePoint(scheduleItem.getApprovePoint());
        if (scheduleItem.getStatus() != -1)
            this.setStatus(scheduleItem.getStatus());
        if (scheduleItem.getSignatureImage() != null && !scheduleItem.getSignatureImage().equals(""))
            this.setSignatureImage(scheduleItem.getSignatureImage());
        if (scheduleItem.getDescription() != null && !scheduleItem.getDescription().equals(""))
            this.setDescription(scheduleItem.getDescription());
    }
}
