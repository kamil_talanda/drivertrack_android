package com.bluetill.drivertrack_xt.schedule.list.filter;

import android.widget.Filter;
import com.bluetill.drivertrack_xt.schedule.ItemVars;
import com.bluetill.drivertrack_xt.schedule.list.mainList.MainLazyAdapter;
import com.bluetill.drivertrack_xt.schedule.ScheduleItem;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 15/01/14
 * Time: 06:30
 * To change this template use File | Settings | File Templates.
 */
public class ScheduleListFilter extends Filter implements ItemVars {

    private final MainLazyAdapter context;

    public ScheduleListFilter(MainLazyAdapter context) {
        this.context = context;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        ScheduleListFilterItem filterItem = ScheduleListFilterItem.valueOf(constraint.toString());
        FilterResults results = new FilterResults();
        if (constraint == null || constraint.length() == 0) {
            results.values = this.context.getData();
            results.count = this.context.getData().size();
        } else {
            ArrayList<ScheduleItem> scheduleItemList = new ArrayList<ScheduleItem>();
            for (ScheduleItem scheduleItem : (ArrayList<ScheduleItem>)this.context.getData()) {
                for (Integer status : filterItem.getStatusList())
                    if (scheduleItem.getStatus() == status) {
                        scheduleItemList.add(scheduleItem);
                    }
            }
            results.values = scheduleItemList;
            results.count = scheduleItemList.size();
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        this.context.setData((ArrayList<ScheduleItem>) results.values);
        context.notifyDataSetChanged();
    }

}
