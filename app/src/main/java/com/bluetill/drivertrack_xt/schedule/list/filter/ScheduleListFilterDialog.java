package com.bluetill.drivertrack_xt.schedule.list.filter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import com.bluetill.drivertrack_xt.R;
import com.bluetill.drivertrack_xt.schedule.ItemStatus;
import com.bluetill.drivertrack_xt.schedule.ItemVars;
import com.bluetill.drivertrack_xt.schedule.list.ScheduleListBaseActivity;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 15/01/14
 * Time: 10:58
 * To change this template use File | Settings | File Templates.
 */
public class ScheduleListFilterDialog extends DialogFragment implements ItemVars {
    ArrayList<Integer> selectedItems = new ArrayList();
    ScheduleListBaseActivity context;

    public void setContext(ScheduleListBaseActivity context) {
        this.context = context;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        boolean inProgressChecked = context.getFilterItem().getStatusList().contains(ItemStatus.IN_PROGRESS.toValue());
        boolean doneChecked = context.getFilterItem().getStatusList().contains(ItemStatus.DONE.toValue());
        boolean[] checkedFilters = {inProgressChecked, doneChecked};
        if(inProgressChecked) selectedItems.add(0);
        if(doneChecked) selectedItems.add(1);
        builder.setTitle(context.getString(R.string.filterTitle))

                .setMultiChoiceItems(R.array.status, checkedFilters,
                        new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which,
                                                boolean isChecked) {
                                if (isChecked) {
                                    selectedItems.add(which);
                                } else if (selectedItems.contains(which)) {
                                    selectedItems.remove(Integer.valueOf(which));
                                }
                            }
                        })
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        final ScheduleListFilterItem filterItem = new ScheduleListFilterItem();
                        for(Integer selectedFilter :selectedItems){
                            switch (selectedFilter){
                                case 0:
                                    filterItem.getStatusList().add(ItemStatus.IN_PROGRESS.toValue());
                                    break;

                                case 1:
                                    filterItem.getStatusList().add(ItemStatus.DONE.toValue());
                                    break;
                                default:
                                    break;
                            }
                        }
                        if(filterItem.getStatusList().size()==0) filterItem.getStatusList().add(ItemStatus.EMPTY.toValue());
                        context.filterList(filterItem);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

        return builder.create();
    }
}