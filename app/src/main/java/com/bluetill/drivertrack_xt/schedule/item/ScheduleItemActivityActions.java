package com.bluetill.drivertrack_xt.schedule.item;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.EditText;

import com.bluetill.drivertrack_xt.Globals;
import com.bluetill.drivertrack_xt.R;
import com.bluetill.drivertrack_xt.Utils;
import com.bluetill.drivertrack_xt.schedule.ItemStatus;
import com.bluetill.drivertrack_xt.schedule.ItemUtils;
import com.bluetill.drivertrack_xt.schedule.ItemVars;
import com.bluetill.drivertrack_xt.schedule.ScheduleItem;
import com.bluetill.drivertrack_xt.schedule.ScheduleService;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 28/01/14
 * Time: 12:56
 * To change this template use File | Settings | File Templates.
 */
public class ScheduleItemActivityActions implements ItemVars {
    ScheduleItemActivity context;

    public ScheduleItemActivityActions(ScheduleItemActivity context) {
        this.context = context;
    }

    protected void initFields(ScheduleItem scheduleItem) {
        context.itemId = scheduleItem.getId();
        context.status = scheduleItem.getStatus();
        context.barcodeImage.setImageBitmap(ScheduleBarcode.createBarcode(scheduleItem.getBarcode(), BARCODE_WIDTH, BARCODE_HEIGHT));
        context.barcodeText.setText(scheduleItem.getBarcode());

        context.holderText.setText(context.getString(R.string.holderName) + " " + scheduleItem.getHolderId());

        context.destinationText.setText(scheduleItem.getDestination());
        setStatusText(scheduleItem.getStatus());
        if (scheduleItem.getSignatureImage() != null) {
            context.signatureImage.setVisibility(View.VISIBLE);
            context.signatureImage.setImageBitmap(Utils.decodeBase64Bmp(scheduleItem.getSignatureImage()));
        }

        if (scheduleItem.getStatus() != ItemStatus.EMPTY.toValue()) {
            if (scheduleItem.getStatus() == ItemStatus.DONE.toValue()) {
                context.statusImage.setImageResource(R.drawable.status_done);
                disableApproveButton();
            } else {
                context.statusImage.setImageResource(R.drawable.status_inprogress);
                context.signatureImage.setVisibility(View.GONE);
            }
        }

        context.descriptionText.setMovementMethod(new ScrollingMovementMethod());
        context.descriptionText.setText(scheduleItem.getDescription());
        if (context.descriptionText.getText().length() < 1) {
            context.descriptionText.setText(context.getString(R.string.addDescription));
        }
        context.addDescriptionButton.setOnClickListener(getClickDescriptionListener());
    }

    protected View.OnClickListener getClickDescriptionListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(context.getClassElement());
                builder.setTitle(context.getString(R.string.addDescriptionTitle));
                final EditText descriptionEdit = new EditText(context.getClassElement());

                builder.setView(descriptionEdit);

                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        addDescription(descriptionEdit.getText().toString());
                    }
                });

                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        };
    }

    protected void addDescription(String descriptionTxt) {
        String addedLine = "\n" + Globals.driverName + ": " + descriptionTxt.trim();
        String changedDescription = context.descriptionText.getText().toString() + addedLine;
        context.descriptionText.setText(changedDescription);
        updateItemCacheList(ITEM_PARAMETER_DESCRIPTION, changedDescription);
        ScheduleService.context = context.getApplicationContext();
        ScheduleService.changeDescriptionLine(context.itemId, addedLine);
    }

    protected void setStatusText(int itemStatus) {
        if (itemStatus == ItemStatus.IN_PROGRESS.toValue())
            context.statusText.setText(ItemStatus.IN_PROGRESS.toString());
        else if (itemStatus == ItemStatus.DONE.toValue())
            context.statusText.setText(ItemStatus.DONE.toString());
        else
            context.statusText.setText(ItemStatus.EMPTY.toString());
    }


    protected void setStatus(ItemStatus status) {
        if (status.equals(ItemStatus.DONE)) {
            context.statusImage.setImageResource(R.drawable.status_done);
            context.statusText.setText(ItemStatus.DONE.toString());
            disableApproveButton();
            context.signatureImage.setVisibility(View.VISIBLE);
        } else {
            context.statusImage.setImageResource(R.drawable.status_inprogress);
            context.statusText.setText(ItemStatus.IN_PROGRESS.toString());
            context.signatureImage.setVisibility(View.GONE);
        }
    }

    protected void setSignatureImage(String imageBase64) {
        context.signatureImage.setImageBitmap(Utils.decodeBase64Bmp(imageBase64));
    }

    protected void enableAssignAction(final ScheduleItem scheduleItem) {
        context.holderText.setText(context.getString(R.string.assignToMe));
        context.holderText.setTextSize(ASSIGN_TEXT_SIZE);
        context.holderText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateItemCacheList(ITEM_PARAMETER_HOLDER, Globals.driverId);
                ScheduleService.setHolder(scheduleItem.getId(), context.getClassElement());
            }
        });
    }

    protected void enableUnassignAction(final ScheduleItem scheduleItem) {
        context.holderText.setText(context.getString(R.string.unassignFromMe));
        context.holderText.setTextSize(ASSIGN_TEXT_SIZE);
        context.holderText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(context.getClassElement());
                builder.setTitle(context.getString(R.string.resetHolderAlertTitle));

                builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        updateItemCacheList(ITEM_PARAMETER_HOLDER, -1);
                        ScheduleService.resetHolder(scheduleItem.getId(), context.getClassElement());
                    }
                });

                builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }

    protected void hideFieldCausedByItemNotFound() {
        context.holderText.setVisibility(View.GONE);
        context.descriptionText.setVisibility(View.GONE);
        context.destinationText.setVisibility(View.GONE);
        context.signatureImage.setVisibility(View.GONE);
        context.statusImage.setVisibility(View.GONE);
        context.statusText.setVisibility(View.GONE);
        context.barcodeImage.setVisibility(View.GONE);
        context.barcodeText.setVisibility(View.GONE);
    }

    protected void disableApproveButton() {
        context.approveButton.setEnabled(false);
        context.approveButton.setBackgroundResource(R.drawable.button_right_pressed);
    }

    protected void approveCurrentItem(String signatureImage) {
        setSignatureImage(signatureImage);
        setStatus(ItemStatus.DONE);
        ArrayList<Integer> tmpItemList = new ArrayList<Integer>();
        tmpItemList.add(context.itemId);
        ItemUtils.approveItemList(signatureImage, tmpItemList, context);
    }

    protected void updateItemCacheList(String name, Object value) {
        for (ScheduleItem scheduleCacheItem : Globals.itemListCache.getItemList()) {
            if (scheduleCacheItem.getId() == context.itemId) {
                Globals.itemListCache.getItemList().remove(scheduleCacheItem);
                ScheduleItem scheduleItem = scheduleCacheItem;
                if (name.equals(ITEM_PARAMETER_STATUS))
                    scheduleItem.setStatus((Integer) value);
                else if (name.equals(ITEM_PARAMETER_DESCRIPTION))
                    scheduleItem.setDescription((String) value);
                else if(name.equals(ITEM_PARAMETER_HOLDER))
                    scheduleItem.setHolderId((Integer)value);
                Globals.itemListCache.getItemList().add(scheduleCacheItem);
            }
        }
    }

}
