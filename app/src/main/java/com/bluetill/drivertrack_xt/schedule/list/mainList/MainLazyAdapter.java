package com.bluetill.drivertrack_xt.schedule.list.mainList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bluetill.drivertrack_xt.Globals;
import com.bluetill.drivertrack_xt.R;
import com.bluetill.drivertrack_xt.schedule.ItemStatus;
import com.bluetill.drivertrack_xt.schedule.ScheduleItem;
import com.bluetill.drivertrack_xt.schedule.list.LazyAdapter;
import com.bluetill.drivertrack_xt.schedule.list.ScheduleListBaseActivity;
import com.bluetill.drivertrack_xt.schedule.list.filter.ScheduleListFilter;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 27/11/13
 * Time: 17:00
 * To change this template use File | Settings | File Templates.
 */
public class MainLazyAdapter extends LazyAdapter implements Filterable {
    private ScheduleListFilter scheduleFilter;

    public MainLazyAdapter(Activity activity, ArrayList<ScheduleItem> data) {
        this.activity = activity;

        ArrayList<ScheduleItem> tmpData = new ArrayList<ScheduleItem>();
        for (ScheduleItem scheduleItem : data) {
            boolean alreadyAdded = false;
            for (ScheduleItem addedItem : tmpData) {
                if (scheduleItem.getBarcode().equals(addedItem.getBarcode())) {
                    alreadyAdded = true;
                }
            }
            if (!alreadyAdded) {
                tmpData.add(new ScheduleItem(scheduleItem));
            } else {

                for (ScheduleItem addedItem : tmpData) {
                    if (scheduleItem.getBarcode().equals(addedItem.getBarcode())) {
                        if (!(addedItem.getStatus() == scheduleItem.getStatus())) {
                            addedItem.setStatus(ItemStatus.EMPTY.toValue());
                        }
                        addedItem.setGroup(true);
                        addedItem.increaseElementsNumber();
                        addedItem.setQty(addedItem.getQty() + scheduleItem.getQty());
                    }
                }
            }
        }

        this.data = tmpData;
        this.inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            view = inflater.inflate(R.layout.list_row, null);
        }

        TextView name = (TextView) view.findViewById(R.id.name);
        TextView destination = (TextView) view.findViewById(R.id.destination);
        TextView qty = (TextView) view.findViewById(R.id.qty);
        CheckBox toApprove = (CheckBox) view.findViewById(R.id.checkbox);
        LinearLayout status = (LinearLayout) view.findViewById(R.id.status);
        TextView approveTime = (TextView) view.findViewById(R.id.approve_time);

        final ScheduleItem item = (ScheduleItem) this.data.get(position);

        name.setText(item.getBarcode());
        destination.setText(item.getDestination());
        qty.setText(Integer.toString(item.getQty()));
        approveTime.setText(timeFormat.format(item.getApproveTime()));
//        qty.setText(Integer.toString(item.getGroupElementsNumber()));

        if (item.getStatus() == ItemStatus.DONE.toValue()) {
            status.setBackgroundColor(activity.getResources().getColor(ItemStatus.DONE.getColorId()));
            toApprove.setEnabled(false);
        } else if (item.getStatus() == ItemStatus.IN_PROGRESS.toValue()) {
            status.setBackgroundColor(activity.getResources().getColor(ItemStatus.IN_PROGRESS.getColorId()));
            toApprove.setEnabled(true);
        } else {
            status.setBackgroundColor(activity.getResources().getColor(ItemStatus.EMPTY.getColorId()));
            toApprove.setEnabled(false);
        }

        if (((ScheduleListBaseActivity) activity).itemsToApprove.contains(item.getId()))
            toApprove.setChecked(true);

        toApprove.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!((ScheduleListBaseActivity) activity).itemsToApprove.contains(item.getId())) {
                    if (isChecked) {
                        for (ScheduleItem scheduleItem : Globals.itemListCache.getItemList()) {
                            if (scheduleItem.getBarcode().equals(item.getBarcode()))
                                ((ScheduleListBaseActivity) activity).itemsToApprove.add(new Integer(scheduleItem.getId()));
                        }
                    }
                } else {
                    if (!isChecked) {
                        for (ScheduleItem scheduleItem : Globals.itemListCache.getItemList()) {
                            if (scheduleItem.getBarcode().equals(item.getBarcode()))
                                ((ScheduleListBaseActivity) activity).itemsToApprove.remove(new Integer(scheduleItem.getId()));
                        }
                    }
                }
            }
        });
        return view;
    }

    @Override
    public Filter getFilter() {
        if (scheduleFilter == null) {
            scheduleFilter = new ScheduleListFilter(this);
        }
        return scheduleFilter;
    }

}
