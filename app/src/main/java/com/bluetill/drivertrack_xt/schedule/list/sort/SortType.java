package com.bluetill.drivertrack_xt.schedule.list.sort;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 29/01/14
 * Time: 16:41
 * To change this template use File | Settings | File Templates.
 */
public enum SortType {
    SORT_BY, BARCODE, RECEIVER, CARRIER, TENANT, APPROVE_TIME, ADD_TIME, DESTINATION, STATUS
}
