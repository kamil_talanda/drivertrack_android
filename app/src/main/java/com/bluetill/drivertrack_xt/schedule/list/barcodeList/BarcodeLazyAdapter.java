package com.bluetill.drivertrack_xt.schedule.list.barcodeList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.bluetill.drivertrack_xt.R;
import com.bluetill.drivertrack_xt.schedule.ItemStatus;
import com.bluetill.drivertrack_xt.schedule.ItemVars;
import com.bluetill.drivertrack_xt.schedule.ScheduleItem;
import com.bluetill.drivertrack_xt.schedule.list.LazyAdapter;
import com.bluetill.drivertrack_xt.schedule.list.ScheduleListBaseActivity;
import com.bluetill.drivertrack_xt.schedule.list.filter.ScheduleListFilter;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 30/01/14
 * Time: 14:19
 * To change this template use File | Settings | File Templates.
 */
public class BarcodeLazyAdapter extends LazyAdapter implements ItemVars{
    private ScheduleListFilter scheduleFilter;

    public BarcodeLazyAdapter(Activity activity, ArrayList<ScheduleItem> data) {
        this.activity = activity;
        this.data = data;
        this.inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            view = inflater.inflate(R.layout.list_row, null);
        }

        TextView name = (TextView) view.findViewById(R.id.name);
        TextView destination = (TextView) view.findViewById(R.id.destination);
        TextView qty = (TextView) view.findViewById(R.id.qty);
        CheckBox toApprove = (CheckBox) view.findViewById(R.id.checkbox);
        LinearLayout status = (LinearLayout) view.findViewById(R.id.status);

        final ScheduleItem item = (ScheduleItem)this.data.get(position);

        name.setText(item.getName());
        destination.setText(item.getDestination());
        qty.setText(Integer.toString(item.getQty()));

        if (item.getStatus() == ItemStatus.DONE.toValue()) {
            status.setBackgroundColor(activity.getResources().getColor(ItemStatus.DONE.getColorId()));
            toApprove.setEnabled(false);
        } else if (item.getStatus() == ItemStatus.IN_PROGRESS.toValue()) {
            status.setBackgroundColor(activity.getResources().getColor(ItemStatus.IN_PROGRESS.getColorId()));
            toApprove.setEnabled(true);
        } else {
            status.setBackgroundColor(activity.getResources().getColor(ItemStatus.EMPTY.getColorId()));
            toApprove.setEnabled(false);
        }

        if (((ScheduleListBaseActivity)activity).itemsToApprove.contains(item.getId())) toApprove.setChecked(true);

        toApprove.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!((ScheduleListBaseActivity)activity).itemsToApprove.contains(item.getId())) {
                    if (isChecked) ((ScheduleListBaseActivity)activity).itemsToApprove.add(item.getId());
                } else {
                    if (!isChecked)
                        ((ScheduleListBaseActivity)activity).itemsToApprove.remove(Integer.valueOf(item.getId()));
                }
            }
        });
        return view;
    }

    @Override
    public Filter getFilter() {
        return null;
    }
}
