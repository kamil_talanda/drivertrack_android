package com.bluetill.drivertrack_xt.schedule.list.barcodeList;

import android.annotation.TargetApi;
import android.os.Build;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;

import com.bluetill.drivertrack_xt.Globals;
import com.bluetill.drivertrack_xt.R;
import com.bluetill.drivertrack_xt.schedule.ItemStatus;
import com.bluetill.drivertrack_xt.schedule.ScheduleItem;
import com.bluetill.drivertrack_xt.schedule.list.ScheduleListBaseActivity;

import java.util.ArrayList;

/**
 * Created by ktalanda on 13/02/14.
 */
public class PopupBarcodeApprove {

    ScheduleListBaseActivity context;
    View view;


    public PopupBarcodeApprove(ScheduleListBaseActivity context, View view) {
        this.context = context;
        this.view = view;
    }

    public void showPopupLegacy(){
        context.approveManyAction(context.itemsToApprove, false);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void showPopup11() {
        PopupMenu popup = new PopupMenu(context, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.approve_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.approve_selected:
                        context.approveManyAction(context.itemsToApprove, false);
                        return true;
                    case R.id.approve_in_transit:
                        ArrayList<Integer> itemsInProgressList = new ArrayList<Integer>();
                        for (ScheduleItem scheduleItem : Globals.itemListCache.getItemList()) {
                            if (scheduleItem.getStatus() == ItemStatus.IN_PROGRESS.toValue())
                                itemsInProgressList.add(scheduleItem.getId());
                        }
                        context.approveManyAction(itemsInProgressList, false);
                        return true;
                    default:
                        return false;
                }
            }
        });
        popup.show();
    }
}
