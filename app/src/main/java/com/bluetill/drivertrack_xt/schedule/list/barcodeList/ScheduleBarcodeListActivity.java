package com.bluetill.drivertrack_xt.schedule.list.barcodeList;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import com.bluetill.drivertrack_xt.R;
import com.bluetill.drivertrack_xt.schedule.ItemStatus;
import com.bluetill.drivertrack_xt.schedule.ItemUtils;
import com.bluetill.drivertrack_xt.schedule.ItemVars;
import com.bluetill.drivertrack_xt.schedule.ScheduleItem;
import com.bluetill.drivertrack_xt.schedule.item.ScheduleItemActivity;
import com.bluetill.drivertrack_xt.schedule.list.ScheduleListBaseActivity;
import com.bluetill.drivertrack_xt.schedule.list.sort.SortType;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.ArrayList;
import java.util.HashMap;

import static com.bluetill.drivertrack_xt.Globals.*;


/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 30/01/14
 * Time: 08:32
 * To change this template use File | Settings | File Templates.
 */
public class ScheduleBarcodeListActivity extends ScheduleListBaseActivity implements ItemVars {

    {
        if(filterItem.getStatusList().size()==0) {
            filterItem.getStatusList().add(ItemStatus.IN_PROGRESS.toValue());
            filterItem.getStatusList().add(ItemStatus.DONE.toValue());
        }
    }

    private ArrayList<ScheduleItem> itemBarcodeList = new ArrayList<ScheduleItem>();
    private String barcode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);

        rightButton.setText(getString(R.string.scan_button));
        rightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator scanIntegrator = new IntentIntegrator(ScheduleBarcodeListActivity.this);
                scanIntegrator.initiateScan();
            }
        });

        barcode = getIntent().getStringExtra(ITEM_PARAMETER_BARCODE);

        filterButton.setVisibility(View.GONE);

        addToList(barcode);

    }

    @Override
    public void actionOnDataLoadingFinish() {
        setUpListView();
    }

    @Override
    public void setUpListView(){
        for(ScheduleItem scheduleItem :itemListCache.getItemList()){
            boolean contains = false;
            ScheduleItem curItem = null;
            for(ScheduleItem item :itemBarcodeList){
                if(item.getId()==scheduleItem.getId()){
                    contains = true;
                    curItem = item;
                }
            }

            if(scheduleItem.getBarcode().equals(barcode) && !contains)
                itemBarcodeList.add(scheduleItem);
            else if(contains){
                itemBarcodeList.remove(curItem);
                itemBarcodeList.add(scheduleItem);
            }

        }
        final BarcodeLazyAdapter tmpAdapter = new BarcodeLazyAdapter(this, itemBarcodeList);
        itemListView.setAdapter(tmpAdapter);
        itemListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(ScheduleBarcodeListActivity.this, ScheduleItemActivity.class);
                intent.putExtra(ITEM_PARAMETER_ID, ((ScheduleItem)tmpAdapter.getItem(i)).getId());
                startActivityForResult(intent, ITEM_ACTIVITY);
            }
        });

        adapter = tmpAdapter;

        checkInProgress(true);
    }

    @Override
    public void resetListView() {

    }

    @Override
    public void sortList(HashMap<SortType, Boolean> sortBy) {
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanningResult != null && scanningResult.getContents() != null) {
            barcode = scanningResult.getContents();
            addToList(barcode);
        }
        switch (requestCode) {
            case ITEM_ACTIVITY:
                if (resultCode == RESULT_OK) {
                    addToList(barcode);
                }
                if (resultCode == RESULT_CANCELED) {
                }
                break;
            case SIGN_ACTIVITY:
                if (resultCode == RESULT_OK) {
                    ItemUtils.approveItemList(intent.getStringExtra(ITEM_PARAMETER_SIGNATURE_IMAGE), (ArrayList<Integer>) intent.getSerializableExtra(ITEM_PARAMETER_ITEM_LIST), this);
                    cleanUpAfterApproveMany((ArrayList<Integer>) intent.getSerializableExtra(ITEM_PARAMETER_ITEM_LIST));
                }
                if (resultCode == RESULT_CANCELED) {
                }
                break;
            default:
                Log.d(APPTAG,
                        getString(R.string.unknown_activity_error));
                break;
        }
    }
}
