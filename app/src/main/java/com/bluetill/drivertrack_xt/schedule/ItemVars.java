package com.bluetill.drivertrack_xt.schedule;

import com.bluetill.drivertrack_xt.Vars;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 14/01/14
 * Time: 13:45
 * To change this template use File | Settings | File Templates.
 */
public interface ItemVars extends Vars {

    String STATUS_DONE = "Delivered";
    String STATUS_IN_PROGRESS = "In Transit";
    String STATUS_EMPTY = "EMPTY";



    //send WS
    String WS_JSON_OUT_ITEM_ID = "item_id";
    String WS_JSON_OUT_BARCODE = "barcode";
    String WS_JSON_OUT_SIGNATURE_IMAGE = "signature_image";
    String WS_JSON_OUT_POINT = "point";
    String WS_JSON_OUT_DESCRIPTION = "description";
    String WS_JSON_OUT_DRIVER_NAME = "driver_name";
    String WS_JSON_OUT_ANDROID_ID = "android_id";

    //receive WS
    String WS_JSON_IN_ITEM_ID = "id";
    String WS_JSON_IN_BARCODE = "barcode";
    String WS_JSON_IN_CARRIER = "carrier";
    String WS_JSON_IN_TENANT = "tenant";
    String WS_JSON_IN_QTY = "qty";
    String WS_JSON_IN_HOLDER_ID = "holderId";
    String WS_JSON_IN_NAME = "name";
    String WS_JSON_IN_DESTINATION = "destination";
    String WS_JSON_IN_ADD_TIME = "addTime";
    String WS_JSON_IN_APPROVE_TIME = "approveTime";
    String WS_JSON_IN_DELIVERY_TIME_PLANNED = "deliveryTimePlanned";
    String WS_JSON_IN_APPROVE_POINT = "approvePoint";
    String WS_JSON_IN_SIGNATURE = "signatureImage";
    String WS_JSON_IN_STATUS = "status";
    String WS_JSON_IN_DESCRIPTION = "description";

    String WS_JSON_IN_TIME_TIME = "time";

    int ITEM_NOT_FOUND_ID = -1;
    int HOLDER_UNASSIGNED_ID = -1;

    int BARCODE_WIDTH = 140;
    int BARCODE_HEIGHT = 60;

    int ASSIGN_TEXT_SIZE = 20;

    String TIME_FORMAT = "dd/MM/yyyy hh:mm";

    long CACHE_VALID_TIME = 60*1000;   //ms

}
