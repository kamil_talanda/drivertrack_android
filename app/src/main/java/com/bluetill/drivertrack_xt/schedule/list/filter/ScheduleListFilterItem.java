package com.bluetill.drivertrack_xt.schedule.list.filter;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 15/01/14
 * Time: 09:36
 * To change this template use File | Settings | File Templates.
 */
public class ScheduleListFilterItem {
    private ArrayList<Integer> statusList;
    private Date addDate;

    public ScheduleListFilterItem(){
        this.statusList = new ArrayList<Integer>();
    }

    public ScheduleListFilterItem(ArrayList<Integer> statusList){
        this.statusList = statusList;
    }

    public ArrayList<Integer> getStatusList() {
        return statusList;
    }

    public void setStatusList(ArrayList<Integer> statusList) {
        this.statusList = statusList;
    }

    public Date getAddDate() {
        return addDate;
    }

    public void setAddDate(Date addDate) {
        this.addDate = addDate;
    }

    public String toString(){
        String result = null;
        if(this.getStatusList()!=null){
            for(Integer status: this.getStatusList()){
                if(result==null) result = status + " ";
                else  result += status + " ";
            }
        }
        if(this.getAddDate()!=null){
            if(result==null) result = "," + this.getAddDate().toString();
            else result += "," + this.getAddDate().toString();
        }

        return result;
    }

    public static ScheduleListFilterItem valueOf(String filterItemString){
        ScheduleListFilterItem result = new ScheduleListFilterItem();
        String[] filterItemArray = filterItemString.split(",");
        if(filterItemArray[0]!=null){
            for(String filterStatusString : filterItemArray[0].split(" ")){
                result.getStatusList().add(Integer.parseInt(filterStatusString));
            }
        }
        return result;
    }
}
