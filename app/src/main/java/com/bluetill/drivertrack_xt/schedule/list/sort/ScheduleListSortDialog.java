package com.bluetill.drivertrack_xt.schedule.list.sort;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import com.bluetill.drivertrack_xt.R;
import com.bluetill.drivertrack_xt.schedule.ItemVars;
import com.bluetill.drivertrack_xt.schedule.list.ScheduleListBaseActivity;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 29/01/14
 * Time: 11:22
 * To change this template use File | Settings | File Templates.
 */
public class ScheduleListSortDialog extends DialogFragment implements ItemVars {

    public static HashMap<SortType, Boolean> sortBy = new HashMap<SortType, Boolean>();
    private ScheduleListBaseActivity context;
    private ListView sortListView;

    public ScheduleListSortDialog(ScheduleListBaseActivity context){
        this.context = context;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_sort, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);

        sortListView = (ListView) view.findViewById(R.id.sort_list);
        reloadListView();

        builder.setTitle(context.getString(R.string.sortTitle))

                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        context.sortList(sortBy);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

        return builder.create();
    }

    public static void setSortBy(SortType sortType, boolean isGrowing){
        sortBy = new HashMap<SortType, Boolean>();
        sortBy.put(sortType, isGrowing);
    }

    public void reloadListView(){
        SortLazyAdapter adapter = new SortLazyAdapter(context, this);
        sortListView.setAdapter(adapter);
    }


}
