package com.bluetill.drivertrack_xt.schedule;

import com.bluetill.drivertrack_xt.Globals;
import com.bluetill.drivertrack_xt.MainActivity;

/**
 * Created with IntelliJ IDEA.
 * User: ktalanda
 * Date: 04/12/13
 * Time: 09:41
 * To change this template use File | Settings | File Templates.
 */
public abstract class ScheduleActivity extends MainActivity {

    public void setItem(ScheduleItem scheduleItem) {};
    public void actionOnDataLoadingFinish() {};

    protected void reloadItemList(){
        Globals.itemListCache = new ScheduleItemListCache(false);
        ScheduleService.getScheduleItemList(this);
    }

}
